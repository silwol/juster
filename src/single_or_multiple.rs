use std::fmt;

#[derive(Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum SingleOrMultiple<T> {
    Single(T),
    Multiple(Vec<T>),
}

impl<T: Eq> Eq for SingleOrMultiple<T> {}
impl<T: PartialEq> PartialEq for SingleOrMultiple<T> {
    fn eq(&self, other: &Self) -> bool {
        use SingleOrMultiple::*;
        match (self, other) {
            (Single(s), Single(o)) => s.eq(o),
            (Multiple(s), Multiple(o)) => s.eq(o),
            _ => false,
        }
    }
}

impl<T> From<T> for SingleOrMultiple<T> {
    fn from(t: T) -> Self {
        use self::SingleOrMultiple::*;
        Single(t)
    }
}

impl<T> From<Vec<T>> for SingleOrMultiple<T> {
    fn from(t: Vec<T>) -> Self {
        use self::SingleOrMultiple::*;
        if t.len() == 1 {
            Single(t.into_iter().next().unwrap())
        } else {
            Multiple(t)
        }
    }
}

impl<T: fmt::Debug> fmt::Debug for SingleOrMultiple<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::SingleOrMultiple::*;
        match self {
            Single(t) => write!(f, "{:?}", t),
            Multiple(t) => write!(f, "{:?}", t),
        }
    }
}
