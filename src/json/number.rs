use float_ord::FloatOrd;
use serde_json;
use std::cmp::Ordering;
use std::fmt;

#[derive(Clone)]
pub enum Number {
    U64(u64),
    I64(i64),
    F64(f64),
}

#[cfg(test)]
impl Number {
    pub fn from_f64(n: f64) -> Option<Self> {
        if n.is_finite() {
            Some(Number::F64(n))
        } else {
            None
        }
    }
}

impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Number::*;
        match self {
            U64(v) => v.fmt(f),
            I64(v) => v.fmt(f),
            F64(v) => v.fmt(f),
        }
    }
}

impl From<u64> for Number {
    fn from(n: u64) -> Number {
        Number::from(&n)
    }
}

impl<'a> From<&'a u64> for Number {
    fn from(n: &'a u64) -> Number {
        Number::U64(*n)
    }
}

impl From<i32> for Number {
    fn from(n: i32) -> Number {
        Number::from(&n)
    }
}

impl<'a> From<&'a i32> for Number {
    fn from(n: &'a i32) -> Number {
        if *n >= 0i32 {
            Number::U64(*n as u64)
        } else {
            Number::I64(i64::from(*n))
        }
    }
}

impl From<i64> for Number {
    fn from(n: i64) -> Number {
        Number::from(&n)
    }
}

impl<'a> From<&'a i64> for Number {
    fn from(n: &'a i64) -> Number {
        if *n >= 0i64 {
            Number::U64(*n as u64)
        } else {
            Number::I64(*n)
        }
    }
}

impl From<serde_json::Number> for Number {
    fn from(j: serde_json::Number) -> Number {
        Number::from(&j)
    }
}

impl<'a> From<&'a serde_json::Number> for Number {
    fn from(j: &'a serde_json::Number) -> Number {
        if j.is_u64() {
            Number::U64(j.as_u64().unwrap())
        } else if j.is_i64() {
            Number::I64(j.as_i64().unwrap())
        } else {
            Number::F64(j.as_f64().unwrap())
        }
    }
}

fn is_divisable(dividend: f64, divisor: f64, limit: f64) -> bool {
    let divided = dividend / divisor;
    let difference = {
        let rounded = if divided >= 0f64 {
            (divided as u64) as f64
        } else {
            (divided as i64) as f64
        };
        if FloatOrd(rounded) > FloatOrd(divided) {
            rounded - divided
        } else {
            divided - rounded
        }
    };
    FloatOrd(difference) < FloatOrd(limit)
}

impl Number {
    pub fn is_divisable_by(&self, divider: &Self) -> bool {
        let limit = 0.000_000_000_01f64;
        use super::Number::*;
        match (self, divider) {
            (U64(s), U64(o)) if *o > 0 => (s % o) == 0,
            (U64(s), I64(o)) if *o > 0 => ((s % (*o as u64)) == 0),
            (U64(s), F64(o)) if *o > 0f64 => {
                is_divisable(*s as f64, *o, limit)
            }
            (I64(s), U64(o)) => (*s % (*o as i64)) == 0,
            (I64(s), I64(o)) if *o > 0 => (s % o) == 0,
            (I64(s), F64(o)) if *o > 0f64 => {
                is_divisable(*s as f64, *o, limit)
            }
            (F64(s), U64(o)) => is_divisable(*s, *o as f64, limit),
            (F64(s), I64(o)) if *o > 0 => {
                is_divisable(*s, *o as f64, limit)
            }
            (F64(s), F64(o)) if *o > 0f64 => is_divisable(*s, *o, limit),
            _ => false,
        }
    }
}

impl Ord for Number {
    fn cmp(&self, other: &Self) -> Ordering {
        use self::Number::*;
        match (self, other) {
            (U64(s), U64(o)) => s.cmp(&o),
            (U64(s), I64(o)) => {
                if *s > ::std::i64::MAX as u64 {
                    Ordering::Greater
                } else {
                    (*s as i64).cmp(&o)
                }
            }
            (U64(s), F64(o)) => FloatOrd(*s as f64).cmp(&FloatOrd(*o)),
            (I64(s), U64(o)) => {
                if *o > ::std::i64::MAX as u64 {
                    Ordering::Less
                } else {
                    s.cmp(&(*o as i64))
                }
            }
            (I64(s), I64(o)) => s.cmp(&o),
            (I64(s), F64(o)) => FloatOrd(*s as f64).cmp(&FloatOrd(*o)),
            (F64(s), U64(o)) => FloatOrd(*s).cmp(&FloatOrd(*o as f64)),
            (F64(s), I64(o)) => FloatOrd(*s).cmp(&FloatOrd(*o as f64)),
            (F64(s), F64(o)) => FloatOrd(*s).cmp(&FloatOrd(*o)),
        }
    }
}

impl PartialOrd for Number {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Number {}

impl PartialEq for Number {
    fn eq(&self, other: &Self) -> bool {
        use self::Number::*;
        match (self, other) {
            (U64(s), U64(o)) => s.eq(o),
            (U64(s), I64(o)) => {
                if *s > ::std::i64::MAX as u64 {
                    false
                } else {
                    (*s as i64).eq(o)
                }
            }
            (U64(s), F64(o)) => (*s as f64).eq(o),
            (I64(s), U64(o)) => {
                if *o > ::std::i64::MAX as u64 {
                    false
                } else {
                    s.eq(&(*o as i64))
                }
            }
            (I64(s), I64(o)) => s.eq(o),
            (I64(s), F64(o)) => (*s as f64).eq(o),
            (F64(s), U64(o)) => s.eq(&(*o as f64)),
            (F64(s), I64(o)) => s.eq(&(*o as f64)),
            (F64(s), F64(o)) => s.eq(o),
        }
    }
}

#[cfg(test)]
mod divisable_tests {
    use super::Number::*;
    #[test]
    fn test_divisable_by() {
        assert!(F64(100f64).is_divisable_by(&F64(10f64)));
        assert!(U64(100u64).is_divisable_by(&F64(10f64)));
        assert!(I64(100i64).is_divisable_by(&F64(10f64)));
        assert!(I64(-100i64).is_divisable_by(&F64(10f64)));

        assert!(F64(100f64).is_divisable_by(&U64(10u64)));
        assert!(U64(100u64).is_divisable_by(&U64(10u64)));
        assert!(I64(100i64).is_divisable_by(&U64(10u64)));
        assert!(I64(-100i64).is_divisable_by(&U64(10u64)));

        assert!(F64(100f64).is_divisable_by(&I64(10i64)));
        assert!(U64(100u64).is_divisable_by(&I64(10i64)));
        assert!(I64(100i64).is_divisable_by(&I64(10i64)));
        assert!(I64(-100i64).is_divisable_by(&I64(10i64)));

        assert!(!F64(100f64).is_divisable_by(&I64(-10i64)));
        assert!(!U64(100u64).is_divisable_by(&I64(-10i64)));
        assert!(!I64(100i64).is_divisable_by(&I64(-10i64)));
        assert!(!I64(-100i64).is_divisable_by(&I64(-10i64)));

        assert!(!F64(100f64).is_divisable_by(&F64(3f64)));
        assert!(!U64(100u64).is_divisable_by(&F64(3f64)));
        assert!(!I64(100i64).is_divisable_by(&F64(3f64)));
        assert!(!I64(-100i64).is_divisable_by(&F64(3f64)));

        assert!(!F64(100f64).is_divisable_by(&U64(3u64)));
        assert!(!U64(100u64).is_divisable_by(&U64(3u64)));
        assert!(!I64(100i64).is_divisable_by(&U64(3u64)));
        assert!(!I64(-100i64).is_divisable_by(&U64(3u64)));

        assert!(!F64(100f64).is_divisable_by(&I64(3i64)));
        assert!(!U64(100u64).is_divisable_by(&I64(3i64)));
        assert!(!I64(100i64).is_divisable_by(&I64(3i64)));
        assert!(!I64(-100i64).is_divisable_by(&I64(3i64)));

        assert!(!F64(100f64).is_divisable_by(&I64(-3i64)));
        assert!(!U64(100u64).is_divisable_by(&I64(-3i64)));
        assert!(!I64(100i64).is_divisable_by(&I64(-3i64)));
        assert!(!I64(-100i64).is_divisable_by(&I64(-3i64)));

        assert!(F64(100f64).is_divisable_by(&F64(0.2f64)));
        assert!(U64(100u64).is_divisable_by(&F64(0.2f64)));
        assert!(I64(100i64).is_divisable_by(&F64(0.2f64)));
        assert!(I64(-100i64).is_divisable_by(&F64(0.2f64)));

        assert!(!F64(100f64).is_divisable_by(&F64(0.3f64)));
        assert!(!U64(100u64).is_divisable_by(&F64(0.3f64)));
        assert!(!I64(100i64).is_divisable_by(&F64(0.3f64)));
        assert!(!I64(-100i64).is_divisable_by(&F64(0.3f64)));
    }
}
