use joinery::Joinable;
use Regex;

pub struct Pointer {
    parts: Vec<String>,
}

lazy_static! {
    static ref RE: Regex = Regex::new(
        "^(/([\u{00}-\u{2E}\u{30}-\u{7d}\u{7f}-\u{10ffff}]|~[01])*)*$",
    ).unwrap();
}

impl Pointer {
    pub fn new(pointer: &str) -> Self {
        unimplemented!();
    }

    pub fn encoded(&self) -> String {
        self.parts
            .iter()
            .map(|s| s.replace("~1", "/").replace("~0", "~"))
            .join_with("/")
            .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_regex() {
        assert!(RE.is_match("/hello"));
        assert!(RE.is_match("/hello~1xyz/trln~1 3/lhc/"));
        assert!(RE.is_match("/~0"));
        assert!(RE.is_match("/~1"));
    }
}
