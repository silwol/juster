use {checks, serde_json, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub schema: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let (serde_json::Value::Object(o), Some(schema)) =
            (self.json, self.schema)
        {
            for key in o.keys() {
                let json = serde_json::Value::from(key.to_string());

                let check = checks::schema::Checker {
                    schema,
                    json: &json,
                    scope: self.scope,
                };
                check.check(&mut |failed| {
                    Failed::PropertyNameDoesNotMatch {
                        name: key.to_string(),
                        failed: Box::new(failed),
                    }.callback(f);
                });
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {serde_json, Check, Failed, Schema, SchemaDescription, Scope};

    fn check_ok(schema: &Option<Schema>, json: &serde_json::Value) {
        let c = Checker {
            schema,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        schema: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            schema,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_object_and_property_names {
        use super::*;

        #[test]
        fn test_exactly_one() {
            let json = json!({"hello":3});
            let property_names = Schema::Description({
                let mut schema = SchemaDescription::default();
                schema.pattern = Some("^hello$".to_string());
                Box::new(schema)
            });
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_exactly_three() {
            let json = json!({"one":1,"two":"two","three":{}});
            let property_names = Schema::Description({
                let mut schema = SchemaDescription::default();
                schema.pattern = Some("^[a-z]*$".to_string());
                Box::new(schema)
            });
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_some_not_ok() {
            let json =
                json!({"one":1,"two":"two","three":{},"four": "hello"});
            let property_names = Schema::Description({
                let mut schema = SchemaDescription::default();
                schema.pattern = Some("^t[a-z]*$".to_string());
                Box::new(schema)
            });
            let expected = vec![
                Failed::PropertyNameDoesNotMatch {
                    name: "four".to_string(),
                    failed: Box::new(Failed::StringDoesNotMatchPattern {
                        pattern: "^t[a-z]*$".to_string(),
                        found: "four".to_string(),
                        scope: Scope::default(),
                    }),
                },
                Failed::PropertyNameDoesNotMatch {
                    name: "one".to_string(),
                    failed: Box::new(Failed::StringDoesNotMatchPattern {
                        pattern: "^t[a-z]*$".to_string(),
                        found: "one".to_string(),
                        scope: Scope::default(),
                    }),
                },
            ];
            check_not_ok(&Some(property_names), &json, &expected);
        }
    }

    mod with_property_names_other_type {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            let property_names = Schema::Bool(false);
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let property_names = Schema::Bool(false);
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(1.234);
            let property_names = Schema::Bool(false);
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let property_names = Schema::Bool(false);
            check_ok(&Some(property_names), &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let property_names = Schema::Bool(false);
            check_ok(&Some(property_names), &json);
        }
    }
}
