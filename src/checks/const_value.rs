use {serde_json, Check, Failed, Scope};

pub struct Checker<'a> {
    pub const_: &'a Option<serde_json::Value>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let Some(const_) = self.const_ {
            if const_ != self.json {
                Failed::ValueDoesNotMatchExpectedConst {
                    scope: self.scope.clone(),
                    const_: const_.clone(),
                    found: self.json.clone(),
                }.callback(f)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(
        const_: &Option<serde_json::Value>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            const_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        const_: &Option<serde_json::Value>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            const_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_const_ok {
        use super::*;

        #[test]
        fn test_object() {
            let json = json!({"hello":3});
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_object_empty() {
            let json = json!({});
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_array_empty() {
            let json = json!([]);
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(34.56);
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_string_empty() {
            let json = json!("");
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_boolean_true() {
            let json = json!(true);
            check_ok(&Some(json.clone()), &json);
        }

        #[test]
        fn test_boolean_false() {
            let json = json!(false);
            check_ok(&Some(json.clone()), &json);
        }
    }

    mod with_const_same_type_not_ok {
        use super::*;

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let const_ = Some(json!({"hello":34}));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let const_ = Some(json!(["hello"]));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            let const_ = Some(json!("hello world"));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(1.23);
            let const_ = Some(json!(4.56));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let const_ = Some(json!(false));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }
    }

    mod with_const_other_type_not_ok {
        use super::*;

        #[test]
        fn test_object_string() {
            let json = json!({"hello":"world"});
            let const_ = Some(json!("hello"));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_array_number() {
            let json = json!(["hello", "world"]);
            let const_ = Some(json!(4.56));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_string_null() {
            let json = json!("hello");
            let const_ = Some(json!(null));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_integer_null() {
            let json = json!(0);
            let const_ = Some(json!(null));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_number_string() {
            let json = json!(1.23);
            let const_ = Some(json!("hello"));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }

        #[test]
        fn test_boolean_number() {
            let json = json!(true);
            let const_ = Some(json!(1));
            let expected = vec![Failed::ValueDoesNotMatchExpectedConst {
                scope: Scope::default(),
                const_: const_.clone().unwrap(),
                found: json.clone(),
            }];
            check_not_ok(&const_, &json, &expected);
        }
    }

    mod without_const {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&None, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(&None, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(&None, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(&None, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&None, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&None, &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(&None, &json);
        }
    }
}
