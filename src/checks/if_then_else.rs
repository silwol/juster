use {checks, serde_json, BranchCondition, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub if_: &'a Option<Schema>,
    pub then_: &'a Option<Schema>,
    pub else_: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let Some(if_) = self.if_ {
            let if_check = checks::schema::Checker {
                schema: if_,
                json: self.json,
                scope: self.scope,
            };
            let matched = if_check.check_ok();

            match (matched, self.then_, self.else_) {
                (true, Some(then_), _) => {
                    let then_check = checks::schema::Checker {
                        schema: then_,
                        json: self.json,
                        scope: self.scope,
                    };
                    then_check.check(&mut |info| {
                        Failed::MismatchInsideConditionalSchema {
                            conditional: BranchCondition::Then,
                            failed: Box::new(info),
                        }.callback(f);
                    });
                }
                (false, _, Some(else_)) => {
                    let else_check = checks::schema::Checker {
                        schema: else_,
                        json: self.json,
                        scope: self.scope,
                    };
                    else_check.check(&mut |info| {
                        Failed::MismatchInsideConditionalSchema {
                            conditional: BranchCondition::Else,
                            failed: Box::new(info),
                        }.callback(f);
                    });
                }
                _ => {}
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::{Check, Stringify};
    use failed::BranchCondition;
    use {
        serde_json, Failed, Schema, SchemaDescription, Scope, SimpleType,
    };

    fn check_ok(
        if_: &Option<Schema>,
        then_: &Option<Schema>,
        else_: &Option<Schema>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            if_,
            then_,
            else_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        if_: &Option<Schema>,
        then_: &Option<Schema>,
        else_: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            if_,
            then_,
            else_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    fn string_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ = Some(SimpleType::String.into());
            Box::new(schema)
        })
    }

    fn bool_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ = Some(SimpleType::Boolean.into());
            Box::new(schema)
        })
    }

    mod tests {
        use super::*;

        #[test]
        fn test_if_true_only() {
            let json = json!({"hello":3});
            check_ok(&Some(Schema::Bool(true)), &None, &None, &json);
        }

        #[test]
        fn test_if_false_only() {
            let json = json!({"hello":3});
            check_ok(&Some(Schema::Bool(false)), &None, &None, &json);
        }

        #[test]
        fn test_if_true_then_false() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Then,
                failed: Box::new(Failed::NeverValid {
                    scope: Scope::default(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(true)),
                &Some(Schema::Bool(false)),
                &None,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_if_false_else_false() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Else,
                failed: Box::new(Failed::NeverValid {
                    scope: Scope::default(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(false)),
                &None,
                &Some(Schema::Bool(false)),
                &json,
                &expected,
            );
        }

        #[test]
        fn test_if_true_then_string() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Then,
                failed: Box::new(Failed::InvalidType {
                    scope: Scope::default(),
                    found: SimpleType::Object,
                    expected: SimpleType::String.into(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(true)),
                &Some(string_schema()),
                &None,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_if_false_else_string() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Else,
                failed: Box::new(Failed::InvalidType {
                    scope: Scope::default(),
                    found: SimpleType::Object,
                    expected: SimpleType::String.into(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(false)),
                &None,
                &Some(string_schema()),
                &json,
                &expected,
            );
        }

        #[test]
        fn test_if_true_then_string_else_bool() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Then,
                failed: Box::new(Failed::InvalidType {
                    scope: Scope::default(),
                    found: SimpleType::Object,
                    expected: SimpleType::String.into(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(true)),
                &Some(string_schema()),
                &Some(bool_schema()),
                &json,
                &expected,
            );
        }

        #[test]
        fn test_if_false_then_string_else_bool() {
            let json = json!({"hello":3});
            let expected = vec![Failed::MismatchInsideConditionalSchema {
                conditional: BranchCondition::Else,
                failed: Box::new(Failed::InvalidType {
                    scope: Scope::default(),
                    found: SimpleType::Object,
                    expected: SimpleType::Boolean.into(),
                }),
            }];
            check_not_ok(
                &Some(Schema::Bool(false)),
                &Some(string_schema()),
                &Some(bool_schema()),
                &json,
                &expected,
            );
        }
    }
}
