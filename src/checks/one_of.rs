use std::collections::BTreeSet;
use {checks, serde_json, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub schemata: &'a Vec<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if !self.schemata.is_empty() {
            let mut matched = BTreeSet::new();
            for (index, schema) in self.schemata.iter().enumerate() {
                let check = checks::schema::Checker {
                    schema,
                    json: self.json,
                    scope: self.scope,
                };
                if check.check_ok() {
                    matched.insert(index);
                }
            }
            if matched.is_empty() {
                Failed::MatchedNoSchemaInOneOf {
                    scope: self.scope.clone(),
                }.callback(f);
            } else if matched.len() > 1 {
                Failed::MatchedMultipleSchemasInOneOf {
                    scope: self.scope.clone(),
                    schemata: matched,
                }.callback(f);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::{Check, Stringify};
    use {serde_json, Failed, Schema, Scope};

    fn check_ok(schemata: &Vec<Schema>, json: &serde_json::Value) {
        let c = Checker {
            schemata,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        schemata: &Vec<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            schemata,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod tests {
        use super::*;

        #[test]
        fn test_many_true() {
            let json = json!({});
            let expected = vec![Failed::MatchedMultipleSchemasInOneOf {
                schemata: vec![
                    0usize, 1usize, 2usize, 3usize, 4usize, 5usize,
                ].into_iter()
                    .collect(),
                scope: Scope::default(),
            }];
            check_not_ok(
                &vec![
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                ],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_many_true_one_false() {
            let json = json!({});
            let expected = vec![Failed::MatchedMultipleSchemasInOneOf {
                schemata: vec![0usize, 1usize, 2usize, 3usize, 5usize]
                    .into_iter()
                    .collect(),
                scope: Scope::default(),
            }];
            check_not_ok(
                &vec![
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(true).into(),
                ],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_many_false() {
            let json = json!({});
            let expected = vec![Failed::MatchedNoSchemaInOneOf {
                scope: Scope::default(),
            }];
            check_not_ok(
                &vec![
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                ],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_many_false_one_true() {
            let json = json!({});
            check_ok(
                &vec![
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(true).into(),
                ],
                &json,
            );
        }
    }
}
