use {checks, serde_json, Check, Failed, Regex, SchemaDescription, Scope};

pub struct Checker<'a> {
    pub schema_description: &'a SchemaDescription,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        {
            if self.schema_description.ref_.is_some() {
                // TODO: implement $ref handling
                unimplemented!(
                    "{:?}: handling \"$ref\" is not yet implemented",
                    self.scope
                );
            }
        }

        {
            let valid_types = self.schema_description.valid_types();
            let check = checks::types::Checker {
                expected: &valid_types,
                json: self.json,
                scope: self.scope,
            };
            if !check.check_ok_with(f) {
                return;
            }
        }

        {
            use checks::numeric_limit::{Checker, Mode};

            {
                let check = Checker {
                    limit: &self.schema_description.multiple_of,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::MultipleOf,
                };
                check.check(f);
            }
            {
                let check = Checker {
                    limit: &self.schema_description.maximum,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::Maximum,
                };
                check.check(f);
            }
            {
                let check = Checker {
                    limit: &self.schema_description.exclusive_maximum,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::MaximumExclusive,
                };
                check.check(f);
            }

            {
                let check = Checker {
                    limit: &self.schema_description.minimum,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::Minimum,
                };
                check.check(f);
            }
            {
                let check = Checker {
                    limit: &self.schema_description.exclusive_minimum,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::MinimumExclusive,
                };
                check.check(f);
            }
        }

        {
            use checks::string_length::{Checker, Mode};

            {
                let check = Checker {
                    limit: &self.schema_description.max_length,
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::MaxLength,
                };
                check.check(f);
            }
            {
                let check = Checker {
                    limit: &Some(self.schema_description.min_length),
                    json: self.json,
                    scope: self.scope,
                    mode: Mode::MinLength,
                };
                check.check(f);
            }
        }

        {
            use checks::pattern::Checker;

            // TODO: We're recompiling the pattern every time, a cache
            // would probably help with better performance.
            let regex = self
                .schema_description
                .pattern
                .as_ref()
                .map(|p| Regex::new(p).unwrap());
            let check = Checker {
                regex: regex.as_ref(),
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::items::Checker;
            let check = Checker {
                items: &self.schema_description.items,
                json: self.json,
                additional_items: &self
                    .schema_description
                    .additional_items,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::num_items::{Checker, Mode};
            {
                let check = Checker {
                    limit: self.schema_description.max_items,
                    mode: Mode::MaxItems,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }

            {
                let check = Checker {
                    limit: Some(self.schema_description.min_items),
                    mode: Mode::MinItems,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
        }

        {
            use checks::unique_items::Checker;
            let check = Checker {
                unique: self.schema_description.unique_items,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::contains::Checker;
            let check = Checker {
                contains: &self.schema_description.contains,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::num_properties::{Checker, Mode};
            {
                let check = Checker {
                    limit: self.schema_description.max_properties,
                    mode: Mode::MaxProperties,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }

            {
                let check = Checker {
                    limit: Some(self.schema_description.min_properties),
                    mode: Mode::MinProperties,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
        }

        {
            use checks::required::Checker;
            let check = Checker {
                required: &self.schema_description.required,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::properties::Checker;
            let check = Checker {
                properties: &self.schema_description.properties,
                pattern_properties: &self
                    .schema_description
                    .pattern_properties,
                additional_properties: &self
                    .schema_description
                    .additional_properties,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        // TODO: check dependencies

        {
            use checks::property_names::Checker;
            let check = Checker {
                schema: &self.schema_description.property_names,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::const_value::Checker;
            let check = Checker {
                const_: &self.schema_description.const_,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        {
            use checks::enum_value::Checker;
            let check = Checker {
                enum_: &self.schema_description.enum_,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }

        // TODO: check format
        // TODO: check content_media_type
        // TODO: check content_encoding
        {
            use checks::if_then_else::Checker;
            let check = Checker {
                if_: &self.schema_description.if_,
                then_: &self.schema_description.then_,
                else_: &self.schema_description.else_,
                json: self.json,
                scope: self.scope,
            };
            check.check(f);
        }
        {
            {
                use checks::all_of::Checker;
                let check = Checker {
                    schemata: &self.schema_description.all_of,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
            {
                use checks::any_of::Checker;
                let check = Checker {
                    schemata: &self.schema_description.any_of,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
            {
                use checks::one_of::Checker;
                let check = Checker {
                    schemata: &self.schema_description.one_of,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
            {
                use checks::not::Checker;
                let check = Checker {
                    schema: &self.schema_description.not,
                    json: self.json,
                    scope: self.scope,
                };
                check.check(f);
            }
        }
    }
}
