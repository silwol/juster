use std::collections::{BTreeMap, BTreeSet};
use {serde_json, Check, Failed, Scope};

pub struct Checker<'a> {
    pub unique: Option<bool>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

fn find_duplicates(v: &[serde_json::Value]) -> BTreeSet<BTreeSet<usize>> {
    let mut result = BTreeMap::new();

    for (index, item) in v.iter().enumerate() {
        if let Some((k, _)) =
            v[0..index].iter().enumerate().find(|(_, v)| *v == item)
        {
            let mut entry = result.entry(k).or_insert_with(BTreeSet::new);
            entry.insert(k);
            entry.insert(index);
        }
    }
    result.into_iter().map(|(_, v)| v).collect()
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        if let (Some(true), Value::Array(v)) = (self.unique, self.json) {
            for duplicates in find_duplicates(v) {
                Failed::DuplicateItemsFoundInUniqueArray {
                    indices: duplicates.clone(),
                    scope: self.scope.clone(),
                }.callback(f)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(unique: Option<bool>, json: &serde_json::Value) {
        let c = Checker {
            unique,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        unique: Option<bool>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            unique,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_array {
        use super::*;

        #[test]
        fn test_empty() {
            let json = json!([]);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_unique() {
            let json = json!(["hello", "world", "how", "are", "you"]);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_some_duplicates() {
            let json = json!([
                "hello", "you", "world", "are", "how", "are", "you", "you"
            ]);
            let expected = vec![
                Failed::DuplicateItemsFoundInUniqueArray {
                    scope: Scope::default(),
                    indices: vec![1, 6, 7].into_iter().collect(),
                },
                Failed::DuplicateItemsFoundInUniqueArray {
                    scope: Scope::default(),
                    indices: vec![3, 5].into_iter().collect(),
                },
            ];
            check_not_ok(Some(true), &json, &expected);
        }

        #[test]
        fn test_only_duplicates() {
            let json =
                json!(["hello", "hello", "hello", "hello", "hello"]);
            let expected =
                vec![Failed::DuplicateItemsFoundInUniqueArray {
                    scope: Scope::default(),
                    indices: vec![0, 1, 2, 3, 4].into_iter().collect(),
                }];
            check_not_ok(Some(true), &json, &expected);
        }

        #[test]
        fn test_duplicate_with_false() {
            let json = json!([
                "hello", "you", "world", "are", "how", "are", "you"
            ]);
            check_ok(Some(false), &json);
        }

        #[test]
        fn test_duplicate_with_none() {
            let json = json!([
                "hello", "you", "world", "are", "how", "are", "you"
            ]);
            check_ok(None, &json);
        }
    }

    mod with_other_type_and_some {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(Some(true), &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(Some(true), &json);
        }
    }

    mod with_other_type_and_none {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(None, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(None, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(None, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(None, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(None, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(None, &json);
        }
    }
}
