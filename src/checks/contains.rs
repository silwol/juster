use {checks, serde_json, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub contains: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        if let (Some(schema), Value::Array(v)) = (self.contains, self.json)
        {
            for json in v {
                let check = checks::schema::Checker {
                    schema: &schema,
                    scope: self.scope,
                    json,
                };
                if check.check_ok() {
                    return;
                }
            }

            {
                Failed::ContainsItemNotPresent {
                    scope: self.scope.clone(),
                }.callback(f);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {
        serde_json, Check, Failed, Schema, SchemaDescription, Scope,
        SimpleType,
    };

    fn check_ok(contains: &Option<Schema>, json: &serde_json::Value) {
        let c = Checker {
            contains,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        contains: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            contains,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    fn number_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ = Some(SimpleType::Number.into());
            Box::new(schema)
        })
    }

    fn string_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ = Some(SimpleType::String.into());
            Box::new(schema)
        })
    }

    fn null_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ = Some(SimpleType::Null.into());
            Box::new(schema)
        })
    }

    fn number_or_null_schema() -> Schema {
        Schema::Description({
            let mut schema = SchemaDescription::default();
            schema.type_ =
                Some(vec![SimpleType::Null, SimpleType::Number].into());
            Box::new(schema)
        })
    }

    mod with_array {
        use super::*;

        #[test]
        fn test_contains_schema_true() {
            let json = json!(["hello", "world"]);
            check_ok(&Some(Schema::Bool(true)), &json);
        }

        #[test]
        fn test_empty_contains_schema_true() {
            let json = json!([]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(Schema::Bool(true)), &json, &expected);
        }

        #[test]
        fn test_contains_schema_false() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(Schema::Bool(false)), &json, &expected);
        }

        #[test]
        fn test_empty_contains_schema_false() {
            let json = json!([]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(Schema::Bool(false)), &json, &expected);
        }

        #[test]
        fn test_empty_contains_number_schema() {
            let json = json!([]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(number_schema()), &json, &expected);
        }

        #[test]
        fn test_absent_contains_number_schema() {
            let json = json!(["hello", "world", true, null, [], {}]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(number_schema()), &json, &expected);
        }

        #[test]
        fn test_present_contains_number_schema() {
            let json = json!(["hello", "world", 7, true, null, [], {}]);
            check_ok(&Some(number_schema()), &json);
        }

        #[test]
        fn test_absent_contains_string_schema() {
            let json = json!([64.42, false, true, null, [], {}]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(string_schema()), &json, &expected);
        }

        #[test]
        fn test_present_contains_string_schema() {
            let json = json!(["hello", "world", 7, true, null, [], {}]);
            check_ok(&Some(string_schema()), &json);
        }

        #[test]
        fn test_absent_contains_null_schema() {
            let json = json!([64.42, false, true, [], {}]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(null_schema()), &json, &expected);
        }

        #[test]
        fn test_present_contains_null_schema() {
            let json = json!(["hello", "world", 7, true, null, [], {}]);
            check_ok(&Some(null_schema()), &json);
        }

        #[test]
        fn test_absent_contains_number_or_null_schema() {
            let json = json!(["hello", "world", true, [], {}]);
            let expected = vec![Failed::ContainsItemNotPresent {
                scope: Scope::default(),
            }];
            check_not_ok(&Some(number_or_null_schema()), &json, &expected);
        }

        #[test]
        fn test_present_null_contains_number_or_null_schema() {
            let json = json!(["hello", "world", true, null, [], {}]);
            check_ok(&Some(number_or_null_schema()), &json);
        }

        #[test]
        fn test_present_number_contains_number_or_null_schema() {
            let json = json!(["hello", "world", true, 66.64, [], {}]);
            check_ok(&Some(number_or_null_schema()), &json);
        }

        #[test]
        fn test_without_contains() {
            let json = json!([]);
            check_ok(&None, &json);
        }
    }

    mod with_other_type_and_some {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&Some(Schema::Bool(false)), &json);
            check_ok(&None, &json);
        }
    }
}
