use simple_type::HasSimpleType;
use {
    serde_json, Check, Failed, Scope, SimpleType, SingleOrMultipleUnique,
};

pub struct Checker<'a> {
    pub expected: &'a SingleOrMultipleUnique<SimpleType>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        let found = self.json.simple_type();
        if !self.expected.contains(&found) {
            Failed::InvalidType {
                scope: self.scope.clone(),
                found,
                expected: self.expected.clone(),
            }.callback(f);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {
        serde_json, Check, Failed, Scope, SimpleType,
        SingleOrMultipleUnique,
    };

    fn check_ok(
        expected: &SingleOrMultipleUnique<SimpleType>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            expected,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        expected: &SingleOrMultipleUnique<SimpleType>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            expected,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod allow_all {
        use super::*;
        fn types() -> SingleOrMultipleUnique<SimpleType> {
            vec![
                SimpleType::Array,
                SimpleType::Boolean,
                SimpleType::Integer,
                SimpleType::Null,
                SimpleType::Number,
                SimpleType::Object,
                SimpleType::String,
            ].into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&types(), &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_string() {
            let json = json!("the world");
            check_ok(&types(), &json);
        }

        #[test]
        fn test_float() {
            let json = json!(6.54f64);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(55u64);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(&types(), &json);
        }
    }

    mod allow_object {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::Object.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&types(), &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Integer,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::Object.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_array {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::Array.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Integer,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::Array.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_boolean {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::Boolean.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Integer,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::Boolean.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_string {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::String.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            check_ok(&types(), &json);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Integer,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::String.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_null {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::Null.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Integer,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::Null.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_integer {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            SimpleType::Integer.into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Number,
                expected: SimpleType::Integer.into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }
    }

    mod allow_integer_and_number {
        use super::*;

        fn types() -> SingleOrMultipleUnique<SimpleType> {
            vec![SimpleType::Number, SimpleType::Integer].into()
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Object,
                expected: vec![SimpleType::Integer, SimpleType::Number]
                    .into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Array,
                expected: vec![SimpleType::Integer, SimpleType::Number]
                    .into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Boolean,
                expected: vec![SimpleType::Integer, SimpleType::Number]
                    .into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::String,
                expected: vec![SimpleType::Integer, SimpleType::Number]
                    .into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let expected = vec![Failed::InvalidType {
                scope: Scope::default(),
                found: SimpleType::Null,
                expected: vec![SimpleType::Integer, SimpleType::Number]
                    .into(),
            }];
            check_not_ok(&types(), &json, &expected);
        }

        #[test]
        fn test_integer() {
            let json = json!(65u64);
            check_ok(&types(), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(65.43f64);
            check_ok(&types(), &json);
        }
    }
}
