use {json, serde_json, Check, Failed, Scope};

pub enum Mode {
    Maximum,
    MaximumExclusive,
    Minimum,
    MinimumExclusive,
    MultipleOf,
}

pub struct Checker<'a> {
    pub limit: &'a Option<serde_json::Number>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
    pub mode: Mode,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        if let (Some(ref limit), Value::Number(v)) =
            (self.limit, self.json)
        {
            use self::Mode::*;
            let limit = json::Number::from(limit);
            let v = json::Number::from(v);
            match self.mode {
                Maximum => {
                    if limit < v {
                        Failed::GreaterThanMaximum {
                            maximum: limit,
                            found: v,
                            scope: self.scope.clone(),
                        }.callback(f);
                    }
                }
                MaximumExclusive => {
                    if limit <= v {
                        Failed::GreaterThanExclusiveMaximum {
                            maximum: limit,
                            found: v,
                            scope: self.scope.clone(),
                        }.callback(f);
                    }
                }
                Minimum => {
                    if limit > v {
                        Failed::LowerThanMinimum {
                            minimum: limit,
                            found: v,
                            scope: self.scope.clone(),
                        }.callback(f);
                    }
                }
                MinimumExclusive => {
                    if limit >= v {
                        Failed::LowerThanExclusiveMinimum {
                            minimum: limit,
                            found: v,
                            scope: self.scope.clone(),
                        }.callback(f);
                    }
                }
                MultipleOf => {
                    if limit > json::Number::from(0u64)
                        && !v.is_divisable_by(&limit)
                    {
                        Failed::NotMultipleOf {
                            multiple_of: limit,
                            found: v,
                            scope: self.scope.clone(),
                        }.callback(f);
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Checker, Mode};
    use checks::Stringify;
    use {json, serde_json, Check, Failed, Scope};

    fn check_ok(
        limit: &Option<serde_json::Number>,
        mode: Mode,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            limit,
            json,
            scope: &Scope::default(),
            mode,
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        limit: &Option<serde_json::Number>,
        mode: Mode,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            limit,
            json,
            scope: &Scope::default(),
            mode,
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_number_maximum_inclusive {
        use super::*;

        const MODE: Mode = Mode::Maximum;

        #[test]
        fn test_1_max_1() {
            let json = json!(1);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_max_1f64() {
            let json = json!(1);
            check_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
            );
        }

        #[test]
        fn test_1_00000000001f64_max_1f64() {
            let json = json!(1.00000000001f64);
            let expected = vec![Failed::GreaterThanMaximum {
                scope: Scope::default(),
                maximum: json::Number::from_f64(1f64).unwrap(),
                found: json::Number::from_f64(1.00000000001f64).unwrap(),
            }];
            check_not_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_1_max_5() {
            let json = json!(1);
            check_ok(&Some(5.into()), MODE, &json);
        }

        #[test]
        fn test_2_max_1() {
            let json = json!(2);
            let expected = vec![Failed::GreaterThanMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(1),
                found: json::Number::from(2),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_0_max_negative_1() {
            let json = json!(0);
            let expected = vec![Failed::GreaterThanMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(-1),
                found: json::Number::from(0),
            }];
            check_not_ok(&Some((-1i64).into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_2() {
            let json = json!(5);
            let expected = vec![Failed::GreaterThanMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(2),
                found: json::Number::from(5),
            }];
            check_not_ok(&Some(2.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_none() {
            let json = json!(5);
            check_ok(&None, MODE, &json);
        }
    }

    mod with_number_maximum_exclusive {
        use super::*;
        const MODE: Mode = Mode::MaximumExclusive;

        #[test]
        fn test_1_max_1() {
            let json = json!(1);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: 1.into(),
                found: 1.into(),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_1_max_1f64() {
            let json = json!(1);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: json::Number::from_f64(1f64).unwrap(),
                found: 1.into(),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_1_00000000001f64_max_1f64() {
            let json = json!(1.00000000001f64);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: json::Number::from_f64(1f64).unwrap(),
                found: json::Number::from_f64(1.00000000001f64).unwrap(),
            }];
            check_not_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_1_max_5() {
            let json = json!(1);
            check_ok(&Some(5.into()), MODE, &json);
        }

        #[test]
        fn test_2_max_1() {
            let json = json!(2);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(1),
                found: json::Number::from(2),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_0_max_negative_1() {
            let json = json!(0);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(-1),
                found: json::Number::from(0),
            }];
            check_not_ok(&Some((-1i64).into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_2() {
            let json = json!(5);
            let expected = vec![Failed::GreaterThanExclusiveMaximum {
                scope: Scope::default(),
                maximum: json::Number::from(2),
                found: json::Number::from(5),
            }];
            check_not_ok(&Some(2.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_none() {
            let json = json!(5);
            check_ok(&None, MODE, &json);
        }
    }

    mod with_number_minimum_inclusive {
        use super::*;

        const MODE: Mode = Mode::Minimum;

        #[test]
        fn test_1_min_1() {
            let json = json!(1);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_min_1f64() {
            let json = json!(1);
            check_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
            );
        }

        #[test]
        fn test_0_99999999999_min_1() {
            let json = json!(0.9999999999f64);
            let expected = vec![Failed::LowerThanMinimum {
                scope: Scope::default(),
                minimum: json::Number::from_f64(1f64).unwrap(),
                found: json::Number::from_f64(0.9999999999f64).unwrap(),
            }];
            check_not_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_5_min_1() {
            let json = json!(5);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_min_2() {
            let json = json!(1);
            let expected = vec![Failed::LowerThanMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(2),
                found: json::Number::from(1),
            }];
            check_not_ok(&Some(2.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_negative_1_min_0() {
            let json = json!(-1);
            let expected = vec![Failed::LowerThanMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(0),
                found: json::Number::from(-1),
            }];
            check_not_ok(&Some(0.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_2_min_5() {
            let json = json!(2);
            let expected = vec![Failed::LowerThanMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(5),
                found: json::Number::from(2),
            }];
            check_not_ok(&Some(5.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_min_none() {
            let json = json!(5);
            check_ok(&None, MODE, &json);
        }
    }

    mod with_number_minimum_exclusive {
        use super::*;
        const MODE: Mode = Mode::MinimumExclusive;

        #[test]
        fn test_1_min_1() {
            let json = json!(1);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: 1.into(),
                found: 1.into(),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_1_min_1f64() {
            let json = json!(1);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: json::Number::from_f64(1f64).unwrap(),
                found: 1.into(),
            }];
            check_not_ok(&Some(1.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_0_9999999999f64_max_1f64() {
            let json = json!(0.9999999999f64);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: json::Number::from_f64(1f64).unwrap(),
                found: json::Number::from_f64(0.9999999999f64).unwrap(),
            }];
            check_not_ok(
                &Some(serde_json::Number::from_f64(1f64).unwrap()),
                MODE,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_5_min_1() {
            let json = json!(5);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_min_2() {
            let json = json!(1);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(2),
                found: json::Number::from(1),
            }];
            check_not_ok(&Some(2.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_negative_1_min_negative_0() {
            let json = json!(-1);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(0),
                found: json::Number::from(-1),
            }];
            check_not_ok(&Some(0.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_2_min_5() {
            let json = json!(2);
            let expected = vec![Failed::LowerThanExclusiveMinimum {
                scope: Scope::default(),
                minimum: json::Number::from(5),
                found: json::Number::from(2),
            }];
            check_not_ok(&Some(5.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_min_none() {
            let json = json!(5);
            check_ok(&None, MODE, &json);
        }
    }

    mod with_number_multiple_of {
        use super::*;
        const MODE: Mode = Mode::MultipleOf;

        #[test]
        fn test_1_multiple_of_0_3() {
            let json = json!(1);
            let expected = vec![Failed::NotMultipleOf {
                scope: Scope::default(),
                multiple_of: json::Number::from_f64(0.3f64).unwrap(),
                found: 1.into(),
            }];
            check_not_ok(
                &Some(serde_json::Number::from_f64(0.3f64).unwrap()),
                MODE,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_1_multiple_of_3() {
            let json = json!(1);
            let expected = vec![Failed::NotMultipleOf {
                scope: Scope::default(),
                multiple_of: 3.into(),
                found: 1.into(),
            }];
            check_not_ok(&Some(3.into()), MODE, &json, &expected);
        }

        #[test]
        fn test_5_multiple_of_1() {
            let json = json!(5);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_5_multiple_of_0_5() {
            let json = json!(1.5);
            check_ok(
                &Some(serde_json::Number::from_f64(0.5f64).unwrap()),
                MODE,
                &json,
            );
        }

        #[test]
        fn test_negative_5_multiple_of_1() {
            let json = json!(-5);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_1_multiple_of_2() {
            let json = json!(1);
            let expected = vec![Failed::NotMultipleOf {
                scope: Scope::default(),
                multiple_of: 2.into(),
                found: 1.into(),
            }];
            check_not_ok(&Some(2.into()), MODE, &json, &expected);
        }
    }

    mod with_other_type_and_some {
        use super::*;
        const MODE: Mode = Mode::Maximum;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_array() {
            let json = json!([10, 20, 30]);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&Some(1.into()), MODE, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&Some(1.into()), MODE, &json);
        }
    }

    mod with_other_type_and_none {
        use super::*;
        const MODE: Mode = Mode::Maximum;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(&None, MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(&None, MODE, &json);
        }

        #[test]
        fn test_array() {
            let json = json!([10, 20, 30]);
            check_ok(&None, MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(&None, MODE, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(&None, MODE, &json);
        }
    }
}
