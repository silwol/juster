use std::collections::BTreeSet;
use {serde_json, Check, Failed, Scope};

pub struct Checker<'a> {
    pub required: &'a BTreeSet<String>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;

        if let Value::Object(o) = self.json {
            self.required
                .iter()
                .filter(|k| !o.contains_key(k.as_str()))
                .for_each(|k| {
                    Failed::RequiredPropertyMissing {
                        scope: self.scope.clone(),
                        property_name: k.to_string(),
                    }.callback(f)
                });
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use std::collections::BTreeSet;
    use {serde_json, Check, Failed, Scope};

    fn check_ok_strvec(required: Vec<&str>, json: &serde_json::Value) {
        check_ok(
            &required.into_iter().map(|s| s.to_string()).collect(),
            json,
        )
    }

    fn check_ok(required: &BTreeSet<String>, json: &serde_json::Value) {
        let c = Checker {
            required,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok_strvec(
        required: Vec<&str>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        check_not_ok(
            &required.into_iter().map(|s| s.to_string()).collect(),
            json,
            expected_messages,
        )
    }

    fn check_not_ok(
        required: &BTreeSet<String>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            required,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_object {
        use super::*;

        #[test]
        fn test_empty_max_none_required() {
            let json = json!({});
            check_ok(&BTreeSet::new(), &json);
        }

        #[test]
        fn test_exact_2() {
            let json = json!({"hello":1,"world":2});
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_more_than_required() {
            let json = json!({"hello":1,"world":{},"how":true,"are":null,"you":[]});
            check_ok_strvec(vec!["hello", "world", "you"], &json);
        }

        #[test]
        fn test_less_than_required() {
            let json = json!({"hello":1, "how":{},"you":null});
            let expected = vec![
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "are".to_string(),
                },
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "world".to_string(),
                },
            ];
            check_not_ok_strvec(
                vec!["hello", "world", "how", "are", "you"],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_missing_from_empty_object() {
            let json = json!({});
            let expected = vec![
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "are".to_string(),
                },
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "hello".to_string(),
                },
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "how".to_string(),
                },
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "world".to_string(),
                },
                Failed::RequiredPropertyMissing {
                    scope: Scope::default(),
                    property_name: "you".to_string(),
                },
            ];
            check_not_ok_strvec(
                vec!["hello", "world", "how", "are", "you"],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_missing_empty_string_from_empty_object() {
            let json = json!({});
            let expected = vec![Failed::RequiredPropertyMissing {
                scope: Scope::default(),
                property_name: "".to_string(),
            }];
            check_not_ok_strvec(vec![""], &json, &expected);
        }
    }

    mod with_other_type_and_required {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok_strvec(vec!["hello", "world"], &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok_strvec(vec!["hello", "world"], &json);
        }
    }

    mod with_other_type_and_empty {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok_strvec(vec![], &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok_strvec(vec![], &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok_strvec(vec![], &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok_strvec(vec![], &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok_strvec(vec![], &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok_strvec(vec![], &json);
        }
    }
}
