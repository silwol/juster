use {serde_json, Check, Failed, Regex, Scope};

pub struct Checker<'a> {
    pub regex: Option<&'a Regex>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        if let (Some(regex), Value::String(s)) = (self.regex, self.json) {
            {
                if !regex.is_match(s) {
                    Failed::StringDoesNotMatchPattern {
                        pattern: regex.as_str().to_string(),
                        found: s.to_string(),
                        scope: self.scope.clone(),
                    }.callback(f);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use regex::Regex;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(regex: Option<&str>, json: &serde_json::Value) {
        let regex = regex.map(|s| Regex::new(s).unwrap());
        let c = Checker {
            regex: regex.as_ref(),
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        regex: Option<&str>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let regex = regex.map(|s| Regex::new(s).unwrap());
        let c = Checker {
            regex: regex.as_ref(),
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_string_regex {
        use super::*;

        #[test]
        fn test_empty() {
            let json = json!("");
            check_ok(Some("^$"), &json);
        }

        #[test]
        fn test_empty_fail() {
            let json = json!("");
            let expected = vec![Failed::StringDoesNotMatchPattern {
                scope: Scope::default(),
                pattern: "^abcd$".to_string(),
                found: "".to_string(),
            }];
            check_not_ok(Some("^abcd$"), &json, &expected);
        }

        #[test]
        fn test_uuid() {
            let json = json!("ebac4301-f006-4bc1-b117-3083f3783b03");
            check_ok(
                Some(
                    "^\
                     [a-fA-F0-9]{8}-\
                     [a-fA-F0-9]{4}-\
                     [a-fA-F0-9]{4}-\
                     [a-fA-F0-9]{4}-\
                     [a-fA-F0-9]{12}$",
                ),
                &json,
            );
        }

        #[test]
        fn test_uuid_fail() {
            let pattern = "^\
                           [a-fA-F0-9]{8}-\
                           [a-fA-F0-9]{4}-\
                           [a-fA-F0-9]{4}-\
                           [a-fA-F0-9]{4}-\
                           [a-fA-F0-9]{12}$";
            let json = json!("hello world i am not a uuid");
            let expected = vec![Failed::StringDoesNotMatchPattern {
                scope: Scope::default(),
                pattern: pattern.to_string(),
                found: "hello world i am not a uuid".to_string(),
            }];
            check_not_ok(Some(pattern), &json, &expected);
        }
    }

    mod with_other_type_and_some {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(Some("^abcd$"), &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(Some("^abcd$"), &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(Some("^abcd$"), &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(Some("^abcd$"), &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(Some("^abcd$"), &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(Some("^abcd$"), &json);
        }
    }

    mod with_other_type_and_none {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(None, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(None, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(None, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(None, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(None, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(None, &json);
        }
    }
}
