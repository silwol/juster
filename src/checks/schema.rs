//! Verify against a JSON schema.

use {checks, serde_json, Check, Failed, Schema, Scope};

/// The checker.
pub struct Checker<'a> {
    /// The schema against which the verification is run.
    pub schema: &'a Schema,
    /// The JSON value that gets checked.
    pub json: &'a serde_json::Value,
    /// The scope of the JSON value.
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        match self.schema {
            Schema::Bool(v) => {
                if !v {
                    Failed::NeverValid {
                        scope: self.scope.clone(),
                    }.callback(f);
                }
            }
            Schema::Description(schema_description) => {
                checks::schema_description::Checker {
                    schema_description,
                    json: self.json,
                    scope: self.scope,
                }.check(f)
            }
        }
    }
}
