//! The collection of all checks that are implemented in the library.

use Failed;

pub mod schema;

mod all_of;
mod any_of;
mod const_value;
mod contains;
mod enum_value;
mod if_then_else;
mod items;
mod not;
mod num_items;
mod num_properties;
mod numeric_limit;
mod one_of;
mod pattern;
mod properties;
mod property_names;
mod required;
mod schema_description;
mod string_length;
mod types;
mod unique_items;

/// A trait implemented by all checks.
pub trait Check {
    /// Run the check, and call f for each failed check.
    fn check(&self, f: &mut FnMut(Failed));

    /// Run the check, and call f for each failed check.
    ///
    /// Returns a flag indicating whether any checks failed.
    fn check_ok_with(&self, f: &mut FnMut(Failed)) -> bool {
        let mut is_ok = true;
        self.check(&mut |failed| {
            is_ok = false;
            f(failed)
        });
        is_ok
    }

    /// Run the check without receiving any details.
    ///
    /// Returns a flag indicating whether any checks failed.
    fn check_ok(&self) -> bool {
        self.check_ok_with(&mut |_| {})
    }
}

#[cfg(test)]
trait Stringify {
    type S;
    fn stringify(self) -> Self::S;
}

#[cfg(test)]
impl<T: ToString> Stringify for Vec<T> {
    type S = Vec<String>;
    fn stringify(self) -> Self::S {
        self.into_iter().map(|t| t.to_string()).collect()
    }
}

#[cfg(test)]
impl<'a, T: ToString> Stringify for &'a Vec<T> {
    type S = Vec<String>;
    fn stringify(self) -> Self::S {
        self.into_iter().map(|t| t.to_string()).collect()
    }
}
