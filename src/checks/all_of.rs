use {checks, serde_json, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub schemata: &'a Vec<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        for (index, schema) in self.schemata.iter().enumerate() {
            let check = checks::schema::Checker {
                schema,
                json: self.json,
                scope: self.scope,
            };
            check.check(&mut |failed| {
                Failed::DidNotMatchSchemaInAllOf {
                    index,
                    failed: Box::new(failed),
                }.callback(f);
            });
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::{Check, Stringify};
    use {serde_json, Failed, Schema, Scope};

    fn check_ok(schemata: &Vec<Schema>, json: &serde_json::Value) {
        let c = Checker {
            schemata,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        schemata: &Vec<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            schemata,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod tests {
        use super::*;

        #[test]
        fn test_many_true() {
            let json = json!({});
            check_ok(
                &vec![
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                ],
                &json,
            );
        }

        #[test]
        fn test_many_true_one_false() {
            let json = json!({});
            let expected = vec![Failed::DidNotMatchSchemaInAllOf {
                index: 4usize,
                failed: Box::new(Failed::NeverValid {
                    scope: Scope::default(),
                }),
            }];
            check_not_ok(
                &vec![
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(true).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(true).into(),
                ],
                &json,
                &expected,
            );
        }

        #[test]
        fn test_many_false() {
            let json = json!({});
            let expected = vec![
                Failed::DidNotMatchSchemaInAllOf {
                    index: 0usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
                Failed::DidNotMatchSchemaInAllOf {
                    index: 1usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
                Failed::DidNotMatchSchemaInAllOf {
                    index: 2usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
                Failed::DidNotMatchSchemaInAllOf {
                    index: 3usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
                Failed::DidNotMatchSchemaInAllOf {
                    index: 4usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
                Failed::DidNotMatchSchemaInAllOf {
                    index: 5usize,
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default(),
                    }),
                },
            ];
            check_not_ok(
                &vec![
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                    Schema::Bool(false).into(),
                ],
                &json,
                &expected,
            );
        }
    }
}
