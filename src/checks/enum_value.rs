use {serde_json, Check, Failed, Scope};

pub struct Checker<'a> {
    pub enum_: &'a Option<Vec<serde_json::Value>>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let Some(enum_) = self.enum_ {
            if !enum_.contains(self.json) {
                Failed::ValueDoesNotMatchEnumValue {
                    scope: self.scope.clone(),
                    enum_: enum_.clone(),
                    found: self.json.clone(),
                }.callback(f)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(
        enum_: &Option<Vec<serde_json::Value>>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            enum_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        enum_: &Option<Vec<serde_json::Value>>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            enum_,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_enum_ok {
        use super::*;

        #[test]
        fn test_object() {
            let json = json!({"hello":3});
            let enum_ = vec![json!({"hello":3}), json!(null)];
            check_ok(&Some(enum_), &json);
        }

        #[test]
        fn test_null() {
            let json = json!(null);
            let enum_ = vec![json!({"hello":3}), json!(null)];
            check_ok(&Some(enum_), &json);
        }

        #[test]
        fn test_object_empty() {
            let json = json!({});
            let enum_ = vec![json!({"hello":3}), json!({}), json!(null)];
            check_ok(&Some(enum_), &json);
        }

        mod with_enum_not_ok {
            use super::*;

            #[test]
            fn test_object() {
                let json = json!({"hello":"world"});
                let enum_ = vec![json!({"hello":34}), json!([])];
                let expected = vec![Failed::ValueDoesNotMatchEnumValue {
                    scope: Scope::default(),
                    enum_: enum_.clone(),
                    found: json.clone(),
                }];
                check_not_ok(&Some(enum_), &json, &expected);
            }

            #[test]
            fn test_number() {
                let json = json!(123);
                let enum_ = vec![json!(1), json!(2), json!(3), json!([])];
                let expected = vec![Failed::ValueDoesNotMatchEnumValue {
                    scope: Scope::default(),
                    enum_: enum_.clone(),
                    found: json.clone().into(),
                }];
                check_not_ok(&Some(enum_), &json, &expected);
            }
        }

        mod without_enum {
            use super::*;

            #[test]
            fn test_null() {
                let json = json!(null);
                check_ok(&None, &json);
            }

            #[test]
            fn test_string() {
                let json = json!("hello");
                check_ok(&None, &json);
            }

            #[test]
            fn test_integer() {
                let json = json!(10);
                check_ok(&None, &json);
            }

            #[test]
            fn test_number() {
                let json = json!(5.42);
                check_ok(&None, &json);
            }

            #[test]
            fn test_boolean() {
                let json = json!(true);
                check_ok(&None, &json);
            }

            #[test]
            fn test_object() {
                let json = json!({"hello":"world"});
                check_ok(&None, &json);
            }

            #[test]
            fn test_array() {
                let json = json!(["hello", "world"]);
                check_ok(&None, &json);
            }
        }
    }
}
