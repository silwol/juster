use std::collections::BTreeMap;
use {checks, serde_json, Check, Failed, Regex, Schema, Scope};

pub struct Checker<'a> {
    pub properties: &'a BTreeMap<String, Schema>,
    pub pattern_properties: &'a BTreeMap<String, Schema>,
    pub additional_properties: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let serde_json::Value::Object(o) = self.json {
            for (key, property) in o.iter() {
                let mut checked = false;
                if let Some(schema) = self.properties.get(key) {
                    let check = checks::schema::Checker {
                        schema,
                        json: property,
                        scope: &self.scope.enter_property(key),
                    };
                    check.check(f);
                    checked = true;
                }

                for (regex, schema) in self.pattern_properties {
                    // TODO: recompiling the regexes is not wise for
                    // performance, we should be caching them somewhere.
                    let regex = Regex::new(regex).unwrap();
                    if regex.is_match(key) {
                        let check = checks::schema::Checker {
                            schema,
                            json: property,
                            scope: &self.scope.enter_property(key),
                        };
                        check.check(&mut |failed| {
                            Failed::PatternPropertyDidNotMatch {
                                pattern: regex.to_string(),
                                failed: Box::new(failed),
                            }.callback(f);
                        });
                        checked = true;
                    }
                }

                if !checked {
                    if let Some(schema) = self.additional_properties {
                        let check = checks::schema::Checker {
                            schema,
                            json: property,
                            scope: &self.scope.enter_property(key),
                        };
                        check.check(&mut |failed| {
                            Failed::AdditionalPropertyFailed {
                                failed: Box::new(failed),
                            }.callback(f);
                        });
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{BTreeMap, Checker};
    use checks::Stringify;
    use {serde_json, Check, Failed, Schema, Scope};

    fn check_ok(
        properties: &BTreeMap<String, Schema>,
        pattern_properties: &BTreeMap<String, Schema>,
        additional_properties: &Option<Schema>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            properties,
            pattern_properties,
            additional_properties,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        properties: &BTreeMap<String, Schema>,
        pattern_properties: &BTreeMap<String, Schema>,
        additional_properties: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            properties,
            pattern_properties,
            additional_properties,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    fn props(p: Vec<(&str, Schema)>) -> BTreeMap<String, Schema> {
        p.into_iter().map(|(k, v)| (k.to_string(), v)).collect()
    }

    mod with_object_and_properties {
        use super::*;

        #[test]
        fn test_exactly_one() {
            let json = json!({"hello":3});
            let properties = props(vec![("hello", Schema::Bool(true))]);
            check_ok(&properties, &BTreeMap::new(), &None, &json);
        }

        #[test]
        fn test_exactly_three() {
            let json = json!({"one":1,"two":"two","three":{}});
            let properties = props(vec![
                ("one", Schema::Bool(true)),
                ("two", Schema::Bool(true)),
                ("three", Schema::Bool(true)),
            ]);
            check_ok(&properties, &BTreeMap::new(), &None, &json);
        }

        #[test]
        fn test_some_overlapping() {
            let json = json!({"one":1,"two":"two"});
            let properties = props(vec![
                ("one", Schema::Bool(true)),
                ("three", Schema::Bool(true)),
            ]);
            check_ok(&properties, &BTreeMap::new(), &None, &json);
        }

        #[test]
        fn test_exact_not_ok() {
            let json = json!({"hello":"world"});
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let expected = vec![Failed::NeverValid {
                scope: Scope::default().enter_property("hello"),
            }];
            check_not_ok(
                &properties,
                &BTreeMap::new(),
                &None,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_properties() {
            let json = json!({"hello":"world"});
            check_ok(&BTreeMap::new(), &BTreeMap::new(), &None, &json);
        }
    }

    mod with_object_and_pattern_properties {
        use super::*;

        #[test]
        fn test_exactly_one_matches() {
            let json = json!({"hello":3});
            let pattern_properties = props(vec![
                ("^hello$", Schema::Bool(true)),
                ("^world$", Schema::Bool(false)),
            ]);
            check_ok(&BTreeMap::new(), &pattern_properties, &None, &json);
        }

        #[test]
        fn test_exactly_one_fails() {
            let json = json!({"hello":3});
            let pattern_properties = props(vec![
                ("^h[a-z]*$", Schema::Bool(false)),
                ("^w[a-z]*$", Schema::Bool(true)),
            ]);
            let expected = vec![Failed::PatternPropertyDidNotMatch {
                pattern: "^h[a-z]*$".to_string(),
                failed: Box::new(Failed::NeverValid {
                    scope: Scope::default().enter_property("hello"),
                }),
            }];
            check_not_ok(
                &BTreeMap::new(),
                &pattern_properties,
                &None,
                &json,
                &expected,
            );
        }

        #[test]
        fn test_match_multiple_not_ok() {
            let json = json!({"hello":"world"});
            let pattern_properties = props(vec![
                ("^[a-z]*$", Schema::Bool(false)),
                ("^hello$", Schema::Bool(false)),
            ]);
            let expected = vec![
                Failed::PatternPropertyDidNotMatch {
                    pattern: "^[a-z]*$".to_string(),
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("hello"),
                    }),
                },
                Failed::PatternPropertyDidNotMatch {
                    pattern: "^hello$".to_string(),
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("hello"),
                    }),
                },
            ];
            check_not_ok(
                &BTreeMap::new(),
                &pattern_properties,
                &None,
                &json,
                &expected,
            );
        }
    }

    mod with_object_and_additional_properties {
        use super::*;

        #[test]
        fn test_matches() {
            let json = json!({"hello":3});
            check_ok(
                &BTreeMap::new(),
                &BTreeMap::new(),
                &Some(Schema::Bool(true)),
                &json,
            );
        }

        #[test]
        fn test_exactly_one_fails() {
            let json = json!({"hello":3});
            let expected = vec![Failed::AdditionalPropertyFailed {
                failed: Box::new(Failed::NeverValid {
                    scope: Scope::default().enter_property("hello"),
                }),
            }];
            check_not_ok(
                &BTreeMap::new(),
                &BTreeMap::new(),
                &Some(Schema::Bool(false)),
                &json,
                &expected,
            );
        }
    }

    mod with_object_and_all_property_fields {
        use super::*;

        #[test]
        fn test_fail_additional_properties() {
            let json =
                json!({"hello":1,"world":2,"how":3,"are":4,"you":5});
            let properties = props(vec![("are", Schema::Bool(true))]);
            let pattern_properties =
                props(vec![("^h.*$", Schema::Bool(true))]);

            let expected = vec![
                Failed::AdditionalPropertyFailed {
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("world"),
                    }),
                },
                Failed::AdditionalPropertyFailed {
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("you"),
                    }),
                },
            ];
            check_not_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
                &expected,
            );
        }

        #[test]
        fn test_fail_all_properties() {
            let json =
                json!({"hello":1,"world":2,"how":3,"are":4,"you":5});
            let properties = props(vec![("are", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^h.*$", Schema::Bool(false))]);

            let expected = vec![
                Failed::NeverValid {
                    scope: Scope::default().enter_property("are"),
                },
                Failed::PatternPropertyDidNotMatch {
                    pattern: "^h.*$".to_string(),
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("hello"),
                    }),
                },
                Failed::PatternPropertyDidNotMatch {
                    pattern: "^h.*$".to_string(),
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("how"),
                    }),
                },
                Failed::AdditionalPropertyFailed {
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("world"),
                    }),
                },
                Failed::AdditionalPropertyFailed {
                    failed: Box::new(Failed::NeverValid {
                        scope: Scope::default().enter_property("you"),
                    }),
                },
            ];
            check_not_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
                &expected,
            );
        }
    }

    mod with_properties_other_type {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^.*$", Schema::Bool(false))]);
            check_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
            );
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^.*$", Schema::Bool(false))]);
            check_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
            );
        }

        #[test]
        fn test_number() {
            let json = json!(1.234);
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^.*$", Schema::Bool(false))]);
            check_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
            );
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^.*$", Schema::Bool(false))]);
            check_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
            );
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let properties = props(vec![("hello", Schema::Bool(false))]);
            let pattern_properties =
                props(vec![("^.*$", Schema::Bool(false))]);
            check_ok(
                &properties,
                &pattern_properties,
                &Some(Schema::Bool(false)),
                &json,
            );
        }
    }
}
