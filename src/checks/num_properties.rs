use {serde_json, Check, Failed, Scope};

pub enum Mode {
    MaxProperties,
    MinProperties,
}

pub struct Checker<'a> {
    pub limit: Option<usize>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
    pub mode: Mode,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        match (self.limit, self.json, &self.mode) {
            (Some(limit), Value::Object(o), Mode::MaxProperties)
                if o.len() > limit =>
            {
                Failed::MoreThanMaxProperties {
                    max_properties: limit,
                    found: o.len(),
                    scope: self.scope.clone(),
                }.callback(f);
            }
            (Some(limit), Value::Object(o), Mode::MinProperties)
                if o.len() < limit =>
            {
                Failed::LessThanMinProperties {
                    min_properties: limit,
                    found: o.len(),
                    scope: self.scope.clone(),
                }.callback(f);
            }
            _ => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Checker, Mode};
    use checks::Stringify;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(
        limit: Option<usize>,
        mode: Mode,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            limit,
            mode,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        limit: Option<usize>,
        mode: Mode,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            limit,
            mode,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_object_max_properties {
        use super::*;

        const MODE: Mode = Mode::MaxProperties;

        #[test]
        fn test_empty_max_0() {
            let json = json!({});
            check_ok(Some(0), MODE, &json);
        }

        #[test]
        fn test_1_max_5() {
            let json = json!({"hello":1});
            check_ok(Some(5), MODE, &json);
        }

        #[test]
        fn test_1_max_1() {
            let json = json!({"hello":1});
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_2_max_1() {
            let json = json!({"hello":1, "world":2});
            let expected = vec![Failed::MoreThanMaxProperties {
                scope: Scope::default(),
                max_properties: 1,
                found: 2,
            }];
            check_not_ok(Some(1), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_2() {
            let json = json!({"hello":1, "world":"xx", "how":{}, "are":[], "you":null});
            let expected = vec![Failed::MoreThanMaxProperties {
                scope: Scope::default(),
                max_properties: 2,
                found: 5,
            }];
            check_not_ok(Some(2), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_none() {
            let json = json!({"hello":1, "world":"xx", "how":{}, "are":{}, "you":null});
            check_ok(None, MODE, &json);
        }
    }

    mod with_object_min_properties {
        use super::*;

        const MODE: Mode = Mode::MinProperties;

        #[test]
        fn test_empty_min_0() {
            let json = json!({});
            check_ok(Some(0), MODE, &json);
        }

        #[test]
        fn test_5_min_1() {
            let json = json!({"hello":1, "world":"xx", "a":{}, "b":[], "c":false});
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_1_min_1() {
            let json = json!({ "hello": null });
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_1_min_2() {
            let json = json!({"hello":[]});
            let expected = vec![Failed::LessThanMinProperties {
                scope: Scope::default(),
                min_properties: 2,
                found: 1,
            }];
            check_not_ok(Some(2), MODE, &json, &expected);
        }

        #[test]
        fn test_2_min_5() {
            let json = json!({"hello":null, "world":true});
            let expected = vec![Failed::LessThanMinProperties {
                scope: Scope::default(),
                min_properties: 5,
                found: 2,
            }];
            check_not_ok(Some(5), MODE, &json, &expected);
        }
    }

    mod with_other_type_and_some {
        use super::*;

        const MODE: Mode = Mode::MaxProperties;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(Some(1), MODE, &json);
        }
    }

    mod with_other_type_and_none {
        use super::*;

        const MODE: Mode = Mode::MaxProperties;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_array() {
            let json = json!(["hello", "world"]);
            check_ok(None, MODE, &json);
        }
    }
}
