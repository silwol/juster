use {checks, serde_json, Check, Failed, Schema, Scope, SingleOrMultiple};

pub struct Checker<'a> {
    pub items: &'a Option<SingleOrMultiple<Schema>>,
    pub additional_items: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Checker<'a> {
    fn get_schema(&self, index: usize) -> Option<&'a Schema> {
        use SingleOrMultiple::*;
        match (self.items, self.additional_items) {
            (Some(Single(schema)), _) => Some(schema),
            (Some(Multiple(schemata)), _) if index < schemata.len() => {
                Some(&schemata[index])
            }
            (Some(Multiple(schemata)), Some(additional))
                if index >= schemata.len() =>
            {
                Some(additional)
            }
            _ => None,
        }
    }
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let serde_json::Value::Array(a) = self.json {
            for (index, json) in a.iter().enumerate() {
                if let Some(schema) = self.get_schema(index) {
                    let check = checks::schema::Checker {
                        schema,
                        json,
                        scope: &self.scope.enter_index(index),
                    };
                    check.check(f);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::Stringify;
    use {serde_json, Check, Failed, Schema, Scope, SingleOrMultiple};

    fn check_ok(
        items: &Option<SingleOrMultiple<Schema>>,
        additional_items: &Option<Schema>,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            items,
            additional_items,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        items: &Option<SingleOrMultiple<Schema>>,
        additional_items: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            items,
            additional_items,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_array {
        use super::*;

        #[test]
        fn test_all_valid_items() {
            let json = json!(["hello", 3]);
            let items = Some(Schema::Bool(true).into());
            let additional_items = None;
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_exact_not_ok() {
            let json = json!(["hello", "world"]);
            let items = Some(Schema::Bool(false).into());
            let additional_items = None;
            let expected = vec![
                Failed::NeverValid {
                    scope: Scope::default().enter_index(0),
                },
                Failed::NeverValid {
                    scope: Scope::default().enter_index(1),
                },
            ];
            check_not_ok(&items, &additional_items, &json, &expected);
        }

        #[test]
        fn test_exact_half_ok() {
            let json = json!(["hello", "world", true, false]);
            let items = Some(
                vec![
                    Schema::Bool(true),
                    Schema::Bool(false),
                    Schema::Bool(true),
                    Schema::Bool(false),
                ].into(),
            );
            let additional_items = None;
            let expected = vec![
                Failed::NeverValid {
                    scope: Scope::default().enter_index(1),
                },
                Failed::NeverValid {
                    scope: Scope::default().enter_index(3),
                },
            ];
            check_not_ok(&items, &additional_items, &json, &expected);
        }

        #[test]
        fn test_some_ok_and_additional_ignored_ok() {
            let json = json!(["hello", "world", true, false]);
            let items =
                Some(vec![Schema::Bool(true), Schema::Bool(true)].into());
            let additional_items = None;
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_additional_not_ok() {
            let json = json!(["hello", "world", true, false]);
            let items =
                Some(vec![Schema::Bool(true), Schema::Bool(true)].into());
            let additional_items = Some(Schema::Bool(false).into());
            let expected = vec![
                Failed::NeverValid {
                    scope: Scope::default().enter_index(2),
                },
                Failed::NeverValid {
                    scope: Scope::default().enter_index(3),
                },
            ];
            check_not_ok(&items, &additional_items, &json, &expected);
        }

        #[test]
        fn test_additional_ok() {
            let json = json!(["hello", "world", true, false]);
            let items = Some(
                vec![Schema::Bool(false), Schema::Bool(false)].into(),
            );
            let additional_items = Some(Schema::Bool(true).into());
            let expected = vec![
                Failed::NeverValid {
                    scope: Scope::default().enter_index(0),
                },
                Failed::NeverValid {
                    scope: Scope::default().enter_index(1),
                },
            ];
            check_not_ok(&items, &additional_items, &json, &expected);
        }

        #[test]
        fn test_with_items_and_superfluous_additional_items() {
            let json = json!(["hello", "world", true, false]);
            let items = Some(Schema::Bool(true).into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_without_items_and_superfluous_additional_items() {
            let json = json!(["hello", "world", true, false]);
            let items = None;
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }
    }

    mod with_other_types {
        use super::*;

        #[test]
        fn test_null() {
            let json = json!(null);
            let items = Some(vec![Schema::Bool(false)].into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":1, "world":[]});
            let items = Some(vec![Schema::Bool(false)].into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(1.234);
            let items = Some(vec![Schema::Bool(false)].into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello world");
            let items = Some(vec![Schema::Bool(false)].into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            let items = Some(vec![Schema::Bool(false)].into());
            let additional_items = Some(Schema::Bool(false).into());
            check_ok(&items, &additional_items, &json);
        }
    }
}
