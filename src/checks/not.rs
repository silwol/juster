use {checks, serde_json, Check, Failed, Schema, Scope};

pub struct Checker<'a> {
    pub schema: &'a Option<Schema>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        if let Some(schema) = self.schema {
            let not_check = checks::schema::Checker {
                schema,
                json: self.json,
                scope: self.scope,
            };
            if not_check.check_ok() {
                Failed::MatchedNotSchema {
                    scope: self.scope.clone(),
                }.callback(f);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Checker;
    use checks::{Check, Stringify};
    use {serde_json, Failed, Schema, Scope};

    fn check_ok(schema: &Option<Schema>, json: &serde_json::Value) {
        let c = Checker {
            schema,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        schema: &Option<Schema>,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            schema,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod tests {
        use super::*;

        #[test]
        fn test_none() {
            let json = json!({});
            check_ok(&None, &json);
        }

        #[test]
        fn test_false() {
            let json = json!({});
            check_ok(&Some(Schema::Bool(false).into()), &json);
        }

        #[test]
        fn test_true() {
            let json = json!({});
            let expected = vec![Failed::MatchedNotSchema {
                scope: Scope::default(),
            }];
            check_not_ok(
                &Some(Schema::Bool(true).into()),
                &json,
                &expected,
            );
        }
    }
}
