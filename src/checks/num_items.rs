use {serde_json, Check, Failed, Scope};

pub enum Mode {
    MaxItems,
    MinItems,
}

pub struct Checker<'a> {
    pub limit: Option<usize>,
    pub json: &'a serde_json::Value,
    pub scope: &'a Scope,
    pub mode: Mode,
}

impl<'a> Check for Checker<'a> {
    fn check(&self, f: &mut FnMut(Failed)) {
        use serde_json::value::Value;
        match (self.limit, self.json, &self.mode) {
            (Some(limit), Value::Array(v), Mode::MaxItems)
                if v.len() > limit =>
            {
                Failed::MoreThanMaxItems {
                    max_items: limit,
                    found: v.len(),
                    scope: self.scope.clone(),
                }.callback(f);
            }
            (Some(limit), Value::Array(v), Mode::MinItems)
                if v.len() < limit =>
            {
                Failed::LessThanMinItems {
                    min_items: limit,
                    found: v.len(),
                    scope: self.scope.clone(),
                }.callback(f);
            }
            _ => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Checker, Mode};
    use checks::Stringify;
    use {serde_json, Check, Failed, Scope};

    fn check_ok(
        limit: Option<usize>,
        mode: Mode,
        json: &serde_json::Value,
    ) {
        let c = Checker {
            limit,
            mode,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let expected = outcome.clone();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected.stringify());
        assert!(ok);
    }

    fn check_not_ok(
        limit: Option<usize>,
        mode: Mode,
        json: &serde_json::Value,
        expected_messages: &Vec<Failed>,
    ) {
        let c = Checker {
            limit,
            mode,
            json,
            scope: &Scope::default(),
        };
        let mut outcome = Vec::new();
        let ok = c.check_ok_with(&mut |o| outcome.push(o));
        assert_eq!(outcome.stringify(), expected_messages.stringify());
        assert!(!ok);
    }

    mod with_array_max_items {
        use super::*;

        const MODE: Mode = Mode::MaxItems;

        #[test]
        fn test_empty_max_0() {
            let json = json!([]);
            check_ok(Some(0), MODE, &json);
        }

        #[test]
        fn test_1_max_5() {
            let json = json!(["hello"]);
            check_ok(Some(5), MODE, &json);
        }

        #[test]
        fn test_1_max_1() {
            let json = json!(["hello"]);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_2_max_1() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::MoreThanMaxItems {
                scope: Scope::default(),
                max_items: 1,
                found: 2,
            }];
            check_not_ok(Some(1), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_2() {
            let json = json!(["hello", "world", "how", "are", "you"]);
            let expected = vec![Failed::MoreThanMaxItems {
                scope: Scope::default(),
                max_items: 2,
                found: 5,
            }];
            check_not_ok(Some(2), MODE, &json, &expected);
        }

        #[test]
        fn test_5_max_none() {
            let json = json!(["hello", "world", "how", "are", "you"]);
            check_ok(None, MODE, &json);
        }
    }

    mod with_array_min_items {
        use super::*;

        const MODE: Mode = Mode::MinItems;

        #[test]
        fn test_empty_min_0() {
            let json = json!([]);
            check_ok(Some(0), MODE, &json);
        }

        #[test]
        fn test_5_min_1() {
            let json = json!(["hello", "world", "a", "b", "c"]);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_1_min_1() {
            let json = json!(["hello"]);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_1_min_2() {
            let json = json!(["hello"]);
            let expected = vec![Failed::LessThanMinItems {
                scope: Scope::default(),
                min_items: 2,
                found: 1,
            }];
            check_not_ok(Some(2), MODE, &json, &expected);
        }

        #[test]
        fn test_2_min_5() {
            let json = json!(["hello", "world"]);
            let expected = vec![Failed::LessThanMinItems {
                scope: Scope::default(),
                min_items: 5,
                found: 2,
            }];
            check_not_ok(Some(5), MODE, &json, &expected);
        }
    }

    mod with_other_type_and_some {
        use super::*;

        const MODE: Mode = Mode::MaxItems;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(Some(1), MODE, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(Some(1), MODE, &json);
        }
    }

    mod with_other_type_and_none {
        use super::*;

        const MODE: Mode = Mode::MaxItems;

        #[test]
        fn test_null() {
            let json = json!(null);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_string() {
            let json = json!("hello");
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_integer() {
            let json = json!(10);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_number() {
            let json = json!(5.42);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_boolean() {
            let json = json!(true);
            check_ok(None, MODE, &json);
        }

        #[test]
        fn test_object() {
            let json = json!({"hello":"world"});
            check_ok(None, MODE, &json);
        }
    }
}
