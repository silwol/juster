#![deny(missing_docs)]

//! A crate providing validation functionality for JSON Schema.

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate serde_derive;

#[cfg(test)]
#[macro_use]
extern crate serde_json;

#[cfg(not(test))]
extern crate serde_json;

extern crate float_ord;
extern crate joinery;
extern crate regex;
extern crate serde;

pub mod checks;

mod failed;
mod json;
mod schema;
mod schema_description;
mod scope;
mod simple_type;
mod single_or_multiple;
mod single_or_multiple_unique;
mod single_or_other_multiple;

use checks::Check;
use failed::{BranchCondition, Failed};
use regex::Regex;
pub use schema::Schema;
pub use schema_description::SchemaDescription;
pub use scope::Scope;
use simple_type::SimpleType;
use single_or_multiple::SingleOrMultiple;
use single_or_multiple_unique::SingleOrMultipleUnique;
use single_or_other_multiple::SingleOrOtherMultiple;

// TODO: 'The "$schema" keyword SHOULD be used in a root schema.'
// TODO: 'It MUST NOT appear in subschemas.'
//
// TODO: Implement a self-check of the schema, so that problems
//       with the schema reveal themselves before causing difficult
//       to explain effects when testing the json value.

/// Perform a simple check, and output the errors to the command-line.
///
/// The error output is printed using eprintln!(), the summary is
/// printed using println!() for now. Note that this behavior might
/// change in future version, we're just exploring the ground right now.
/// The scope used is the default scope.
///
/// Returns the number of failed checks.
pub fn simple_schema_check(
    schema: &Schema,
    json: &serde_json::Value,
) -> usize {
    let check = checks::schema::Checker {
        schema,
        json,
        scope: &Scope::default(),
    };
    let mut failures = 0usize;
    check.check(&mut |failure| {
        eprintln!("{:?}\n", failure);
        failures += 1usize;
    });
    if failures == 0usize {
        println!("Success. All tests passed.");
    } else {
        println!("Failure. {} tests failed.", failures);
    }
    failures
}
