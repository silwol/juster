#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum SingleOrOtherMultiple<S, M> {
    Single(S),
    Multiple(Vec<M>),
}
