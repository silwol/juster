use joinery::Joinable;
use std::fmt;

#[derive(Clone, Eq, PartialEq)]
enum Part {
    Property(String),
    Index(usize),
}

/// The scope of an item in a JSON document.
#[derive(Default, Clone, Eq, PartialEq)]
pub struct Scope {
    parts: Vec<Part>,
}

impl fmt::Debug for Scope {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let path = format!(
            "/{}",
            self.parts
                .iter()
                .map(|p| match p {
                    Part::Property(s) => format!(
                        "/{}",
                        s.replace('/', "~1").replace('~', "~0")
                    ),
                    Part::Index(i) => format!("[{}]", i),
                })
                .join_with("")
        );
        write!(f, "{}", path)
    }
}

impl Scope {
    /// Enter a new property subscope.
    pub fn enter_property(&self, property: &str) -> Self {
        let mut parts = self.parts.to_vec();
        parts.push(Part::Property(property.to_string()));
        Scope { parts }
    }
    /// Enter a new index subscope.
    pub fn enter_index(&self, index: usize) -> Self {
        let mut parts = self.parts.to_vec();
        parts.push(Part::Index(index));
        Scope { parts }
    }
}
