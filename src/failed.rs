use std::collections::BTreeSet;
use std::fmt;
use {json, serde_json, Scope, SimpleType, SingleOrMultipleUnique};

#[derive(Clone, Eq, PartialEq)]
pub enum BranchCondition {
    Then,
    Else,
}

#[derive(Clone)]
pub enum Failed {
    NeverValid {
        scope: Scope,
    },
    InvalidType {
        scope: Scope,
        found: SimpleType,
        expected: SingleOrMultipleUnique<SimpleType>,
    },
    LessThanMinItems {
        scope: Scope,
        min_items: usize,
        found: usize,
    },
    MoreThanMaxItems {
        scope: Scope,
        max_items: usize,
        found: usize,
    },
    LessThanMinProperties {
        scope: Scope,
        min_properties: usize,
        found: usize,
    },
    MoreThanMaxProperties {
        scope: Scope,
        max_properties: usize,
        found: usize,
    },
    GreaterThanMaximum {
        scope: Scope,
        maximum: json::Number,
        found: json::Number,
    },
    GreaterThanExclusiveMaximum {
        scope: Scope,
        maximum: json::Number,
        found: json::Number,
    },
    LowerThanMinimum {
        scope: Scope,
        minimum: json::Number,
        found: json::Number,
    },
    LowerThanExclusiveMinimum {
        scope: Scope,
        minimum: json::Number,
        found: json::Number,
    },
    NotMultipleOf {
        scope: Scope,
        multiple_of: json::Number,
        found: json::Number,
    },
    StringTooLong {
        scope: Scope,
        max_length: usize,
        length: usize,
    },
    StringTooShort {
        scope: Scope,
        min_length: usize,
        length: usize,
    },
    RequiredPropertyMissing {
        scope: Scope,
        property_name: String,
    },
    DuplicateItemsFoundInUniqueArray {
        scope: Scope,
        indices: BTreeSet<usize>,
    },
    ValueDoesNotMatchExpectedConst {
        scope: Scope,
        const_: serde_json::Value,
        found: serde_json::Value,
    },
    ValueDoesNotMatchEnumValue {
        scope: Scope,
        enum_: Vec<serde_json::Value>,
        found: serde_json::Value,
    },
    ContainsItemNotPresent {
        scope: Scope,
    },
    StringDoesNotMatchPattern {
        pattern: String,
        found: String,
        scope: Scope,
    },
    MismatchInsideConditionalSchema {
        conditional: BranchCondition,
        failed: Box<Failed>,
    },
    DidNotMatchSchemaInAllOf {
        index: usize,
        failed: Box<Failed>,
    },
    MatchedNoSchemaInAnyOf {
        scope: Scope,
    },
    MatchedMultipleSchemasInOneOf {
        scope: Scope,
        schemata: BTreeSet<usize>,
    },
    MatchedNoSchemaInOneOf {
        scope: Scope,
    },
    MatchedNotSchema {
        scope: Scope,
    },
    PatternPropertyDidNotMatch {
        pattern: String,
        failed: Box<Failed>,
    },
    AdditionalPropertyFailed {
        failed: Box<Failed>,
    },
    PropertyNameDoesNotMatch {
        name: String,
        failed: Box<Failed>,
    },
}

impl Failed {
    pub fn callback(self, f: &mut FnMut(Failed)) {
        f(self);
    }
}

impl fmt::Debug for Failed {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Failed::*;
        match self {
            NeverValid { scope } => write!(
                f,
                "{:?}: schema is `false`, no valid values possible",
                scope
            ),
            InvalidType {
                scope,
                found,
                expected,
            } => write!(
                f,
                "{:?}: found {:?}, expected {:?}",
                scope, found, expected
            ),
            LessThanMinItems {
                scope,
                found,
                min_items,
            } => write!(
                f,
                "{:?}: at least {} items required, found {}",
                scope, min_items, found
            ),
            MoreThanMaxItems {
                scope,
                found,
                max_items,
            } => write!(
                f,
                "{:?}: at most {} items allowed, found {}",
                scope, max_items, found
            ),
            LessThanMinProperties {
                scope,
                found,
                min_properties,
            } => write!(
                f,
                "{:?}: at least {} properties required, found {}",
                scope, min_properties, found
            ),
            MoreThanMaxProperties {
                scope,
                found,
                max_properties,
            } => write!(
                f,
                "{:?}: at most {} properties allowed, found {}",
                scope, max_properties, found
            ),
            GreaterThanMaximum {
                scope,
                maximum,
                found,
            } => write!(
                f,
                "{:?}: value {} is greater than allowed maximum {}",
                scope, found, maximum
            ),
            GreaterThanExclusiveMaximum {
                scope,
                maximum,
                found,
            } => write!(
                f,
                "{:?}: value {} is greater than or equal to allowed \
                 exclusive maximum {}",
                scope, found, maximum
            ),
            LowerThanMinimum {
                scope,
                minimum,
                found,
            } => write!(
                f,
                "{:?}: value {} is lower than allowed minimum {}",
                scope, found, minimum
            ),
            LowerThanExclusiveMinimum {
                scope,
                minimum,
                found,
            } => write!(
                f,
                "{:?}: value {} is lower than or equal to allowed \
                 exclusive minimum {}",
                scope, found, minimum
            ),
            NotMultipleOf {
                scope,
                multiple_of,
                found,
            } => write!(
                f,
                "{:?}: value {} is not a multiple of {}",
                scope, found, multiple_of
            ),
            StringTooLong {
                scope,
                max_length,
                length,
            } => write!(
                f,
                "{:?}: string with {} characters longer than allowed \
                 maximum {} characters",
                scope, length, max_length
            ),
            StringTooShort {
                scope,
                min_length,
                length,
            } => write!(
                f,
                "{:?}: string with {} characters shorter than allowed \
                 minimum {} characters",
                scope, length, min_length
            ),
            RequiredPropertyMissing {
                scope,
                property_name,
            } => write!(
                f,
                "{:?}: missing required property {:?}",
                scope, property_name,
            ),
            DuplicateItemsFoundInUniqueArray { scope, indices } => write!(
                f,
                "{:?}: unique array contains duplicates at indices {:?}",
                scope, indices
            ),
            ValueDoesNotMatchExpectedConst {
                scope,
                const_,
                found,
            } => write!(
                f,
                "{:?}: value does not match required const value\n\
                 found:\n{:#?}\nrequired const:\n{:#?}",
                scope, found, const_
            ),
            ValueDoesNotMatchEnumValue {
                scope,
                enum_,
                found,
            } => write!(
                f,
                "{:?}: value does not match any of the enum values\n\
                 found:\n{:#?}\nenum values:\n{:#?}",
                scope, found, enum_
            ),
            ContainsItemNotPresent { scope } => write!(
                f,
                "{:?}: item required by `contains` not found in array",
                scope
            ),
            StringDoesNotMatchPattern {
                scope,
                found,
                pattern,
            } => write!(
                f,
                "{:?}: string {:?} does not match regex pattern {:?}",
                scope, found, pattern
            ),
            MismatchInsideConditionalSchema {
                conditional,
                failed,
            } => {
                use self::BranchCondition::*;
                let (matched, branch) = match conditional {
                    Then => ("`if` condition matched", "then"),
                    Else => ("`if` condition did not match", "else"),
                };
                write!(
                    f,
                    "The {}, but the `{}` branch failed:\n{:?}",
                    matched, branch, failed
                )
            }
            DidNotMatchSchemaInAllOf { index, failed } => write!(
                f,
                "Schema at index {} in `allOf` did not match:\n{:?}",
                index, failed
            ),
            MatchedNoSchemaInAnyOf { scope } => write!(
                f,
                "{:?} matched none of the schemata in `anyOf`",
                scope,
            ),
            MatchedMultipleSchemasInOneOf { scope, schemata } => write!(
                f,
                "{:?} matched multiple schemata in `oneOf` at index: {:?}",
                scope, schemata
            ),
            MatchedNoSchemaInOneOf { scope } => write!(
                f,
                "{:?} matched none of the schemata in `oneOf`",
                scope,
            ),
            MatchedNotSchema { scope } => {
                write!(f, "{:?}: matches `not` schema", scope)
            }
            PatternPropertyDidNotMatch { pattern, failed } => write!(
                f,
                "Pattern property matching regex {:?} failed:\n{:?}",
                pattern, failed
            ),
            AdditionalPropertyFailed { failed } => {
                write!(f, "Additional property failed:\n{:?}", failed)
            }
            PropertyNameDoesNotMatch { name, failed } => write!(
                f,
                "Property name {:?} does not match schema:\n{:?}",
                name, failed
            ),
        }
    }
}

impl ToString for Failed {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}
