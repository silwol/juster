use serde_json;

#[derive(Ord, PartialOrd, Eq, PartialEq, Debug, Clone, Serialize,
         Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum SimpleType {
    Array,
    Boolean,
    Integer,
    Null,
    Number,
    Object,
    String,
}

pub trait HasSimpleType {
    fn simple_type(&self) -> SimpleType;
}

impl HasSimpleType for serde_json::Value {
    fn simple_type(&self) -> SimpleType {
        use serde_json::Value;
        match self {
            Value::Null => SimpleType::Null,
            Value::Bool(_) => SimpleType::Boolean,
            Value::Number(n) => {
                if n.is_f64() {
                    SimpleType::Number
                } else {
                    SimpleType::Integer
                }
            }
            Value::String(_) => SimpleType::String,
            Value::Array(_) => SimpleType::Array,
            Value::Object(_) => SimpleType::Object,
        }
    }
}
