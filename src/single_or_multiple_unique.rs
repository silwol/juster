use std::collections::BTreeSet;
use std::fmt;

#[derive(Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum SingleOrMultipleUnique<T: Ord> {
    Single(T),
    Multiple(BTreeSet<T>),
}

impl<T: fmt::Debug + Ord> fmt::Debug for SingleOrMultipleUnique<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::SingleOrMultipleUnique::*;
        match self {
            Single(t) => write!(f, "{:?}", t),
            Multiple(t) => write!(f, "{:?}", t),
        }
    }
}

impl<T: Ord> From<T> for SingleOrMultipleUnique<T> {
    fn from(t: T) -> Self {
        use self::SingleOrMultipleUnique::*;
        Single(t)
    }
}

impl<T: Ord> From<Vec<T>> for SingleOrMultipleUnique<T> {
    fn from(t: Vec<T>) -> Self {
        use self::SingleOrMultipleUnique::*;
        if t.len() == 1 {
            Single(t.into_iter().next().unwrap())
        } else {
            Multiple(t.into_iter().collect())
        }
    }
}

impl<T: Ord> From<BTreeSet<T>> for SingleOrMultipleUnique<T> {
    fn from(t: BTreeSet<T>) -> Self {
        use self::SingleOrMultipleUnique::*;
        if t.len() == 1 {
            Single(t.into_iter().next().unwrap())
        } else {
            Multiple(t.into_iter().collect())
        }
    }
}

impl<T: Ord> SingleOrMultipleUnique<T> {
    pub fn contains(&self, item: &T) -> bool {
        use self::SingleOrMultipleUnique::*;
        match self {
            Single(t) => (t == item),
            Multiple(t) => t.contains(item),
        }
    }
}

impl<T: Eq + Ord> Eq for SingleOrMultipleUnique<T> {}

impl<T: PartialEq + Ord> PartialEq for SingleOrMultipleUnique<T> {
    fn eq(&self, other: &Self) -> bool {
        use self::SingleOrMultipleUnique::*;
        match (self, other) {
            (Single(t), Single(o)) => t.eq(o),
            (Multiple(t), Multiple(o)) => t.eq(o),
            _ => false,
        }
    }
}
