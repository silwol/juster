use SchemaDescription;

/// The schema.
///
/// A JSON schema may either be `true` (accept all values), `false`
/// (reject all values), or a schema description object.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Schema {
    /// A boolean schema.
    Bool(bool),
    /// A full schema description object.
    Description(Box<SchemaDescription>),
}
