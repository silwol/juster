#[macro_use]
extern crate failure;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate structopt;

extern crate colored;
extern crate glob;
extern crate juster;
extern crate serde_json;

use colored::Colorize;
use juster::checks::Check;
use std::ffi::OsStr;
use std::fs::File;
use std::path::Path;
use structopt::StructOpt;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(StructOpt, Debug)]
struct RunTestSuiteArguments {
    /// The path of the test suite.
    /// Should usually point to the `tests/draft7/` subdirectory
    /// of the test suite git repository.
    path: String,
}

#[derive(StructOpt, Debug)]
enum Command {
    #[structopt(name = "run-test-suite")]
    RunTestSuite(RunTestSuiteArguments),
}

#[derive(StructOpt, Debug)]
#[structopt(name = "juster")]
struct Arguments {
    #[structopt(subcommand)]
    command: Command,
}

#[derive(Debug, Serialize, Deserialize)]
struct Test {
    description: String,
    data: serde_json::Value,
    valid: bool,
}

#[derive(Debug, Serialize, Deserialize)]
struct TestSet {
    description: String,
    schema: juster::Schema,
    tests: Vec<Test>,
}

fn run_tests<P: AsRef<Path>>(path: P) -> Result<(String, usize)> {
    let reader = File::open(&path)?;

    let test_sets: Vec<TestSet> = serde_json::from_reader(reader)?;

    let name = path
        .as_ref()
        .file_name()
        .unwrap_or_else(|| path.as_ref().as_os_str())
        .to_string_lossy();
    println!("{}", format!("File {:?}", name).bold());

    let mut errors = 0usize;

    for test_set in test_sets {
        println!("• {}", test_set.description);
        for test in test_set.tests {
            let scope = juster::Scope::default();
            let check = juster::checks::schema::Checker {
                schema: &test_set.schema,
                json: &test.data,
                scope: &scope,
            };

            if check.check_ok() != test.valid {
                errors += 1usize;
                eprintln!("  {} {}", "✗".bold().red(), test.description);
                eprintln!(
                    "    schema: {}",
                    serde_json::to_string(check.schema)?
                );
                eprintln!(
                    "    value: {}",
                    serde_json::to_string(check.json)?
                );
            } else {
                println!(
                    "  {} {}",
                    "✓".bold().green(),
                    test.description
                );
            }
        }
    }
    if errors > 0 {
        eprintln!("{} errors", errors);
    }
    println!("");

    Ok((name.to_string(), errors))
}

fn run_test_suite<P: AsRef<Path>>(path: P) -> Result<()> {
    if !path.as_ref().is_dir() {
        eprintln!(
            "Test suite path {:?} is not a directory.",
            path.as_ref().to_path_buf()
        );
        return Err(format_err!(
            "Test suite path {:?} is not a directory.",
            path.as_ref().to_path_buf()
        ));
    }
    let pattern = path.as_ref().join("*.json");
    println!("{}", format!("Tests pattern: {:?}", pattern).bold());
    println!("");

    let pattern: &OsStr = pattern.as_ref();
    let pattern = pattern.to_str().unwrap();

    let mut summary = Vec::new();

    let mut count = 0;
    for path in glob::glob(pattern)? {
        match path {
            Ok(path) => {
                let info = run_tests(path)?;
                if info.1 > 0usize {
                    summary.push(info);
                }
                count = count + 1;
            }
            Err(e) => {
                eprintln!("Path error: {:?}", e);
            }
        }
    }

    if count == 0 {
        eprintln!("No valid test files found");
        return Err(format_err!("{}", "No valid test files found"));
    } else if !summary.is_empty() {
        eprintln!("{}", "Summary".bold());
        for (name, count) in summary {
            eprintln!(
                "{} {} errors in {}",
                "✗".bold().red(),
                count.to_string().bold(),
                name
            );
        }
        eprintln!();
    }
    Ok(())
}

fn main() -> Result<()> {
    let arguments = Arguments::from_args();

    match arguments.command {
        Command::RunTestSuite(arguments) => {
            run_test_suite(&arguments.path)?;
        }
    }
    Ok(())
}
