use std::collections::{BTreeMap, BTreeSet};
use {
    serde, serde_json, Schema, SimpleType, SingleOrMultiple,
    SingleOrMultipleUnique, SingleOrOtherMultiple,
};

fn is_default<T: Default + PartialEq>(t: &T) -> bool {
    *t == Default::default()
}

impl SchemaDescription {
    /// Get all the types which are allowed for this schema.
    pub fn valid_types(&self) -> SingleOrMultipleUnique<SimpleType> {
        use SingleOrMultipleUnique::*;
        match self.type_ {
            Some(ref t) => match t {
                Single(t) => match t {
                    SimpleType::Number => {
                        vec![t.clone(), SimpleType::Integer].into()
                    }
                    _ => t.clone().into(),
                },
                Multiple(t) => {
                    let mut items = (*t).clone();
                    if items.contains(&SimpleType::Number) {
                        items.insert(SimpleType::Integer);
                    }
                    items.into()
                }
            },
            None => vec![
                SimpleType::Array,
                SimpleType::Boolean,
                SimpleType::Integer,
                SimpleType::Null,
                SimpleType::Number,
                SimpleType::Object,
                SimpleType::String,
            ].into(),
        }
    }
}

// Any value that is present is considered Some value, including null.
fn deserialize_some<'de, T, D>(
    deserializer: D,
) -> Result<Option<T>, D::Error>
where
    T: serde::de::Deserialize<'de>,
    D: serde::de::Deserializer<'de>,
{
    serde::de::Deserialize::deserialize(deserializer).map(Some)
}

/// A full schema description object.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SchemaDescription {
    /// The id of the schema.
    ///
    /// JSON object member: `$id`.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-core.html#rfc.section.8.2
    // TODO: format: uri-reference
    #[serde(
        rename = "$id",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub id: Option<String>,

    /// The uri of the schema.
    ///
    /// JSON object member: `$schema`.
    ///
    /// Must be a RFC3986 URI, but not necessarily resolve to a valid
    /// URL. Must be normalized. Must not appear in subschemas, should
    /// only be used in a root schema.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-core.html#rfc.section.7
    // TODO: format: uri
    #[serde(
        rename = "$schema",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub schema: Option<String>,

    /// A reference to an URI which gets used for the description.
    ///
    /// JSON object member: `$ref`.
    ///
    /// All other properties will be ignored if this value is set.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-core.html#rfc.section.8.3
    // TODO: format: uri-reference
    #[serde(
        rename = "$ref",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub ref_: Option<String>,

    /// A comment containing details about this schema.
    ///
    /// JSON object member: `$comment`.
    ///
    /// Will not be used for verification, is just informative.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-core.html#rfc.section.9
    #[serde(
        rename = "$comment",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub comment: Option<String>,

    /// The title of the schema.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.10.1
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub title: Option<String>,

    /// The description fo the schema.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.10.1
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub description: Option<String>,

    /// The default value.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.10.2
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none",
        deserialize_with = "deserialize_some"
    )]
    pub default: Option<serde_json::Value>,

    /// The readOnly flag.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.10.3
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub read_only: Option<bool>,

    /// Examples of valid instances.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.10.4
    #[serde(
        default = "Default::default", skip_serializing_if = "Vec::is_empty"
    )]
    pub examples: Vec<serde_json::Value>,

    /// Numeric values must be a multiple of this value.
    ///
    /// If set, this must be a strictly positive number.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.2.1
    // TODO: verify that this is a strict positive number (>0)…
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub multiple_of: Option<serde_json::Number>,

    /// The maximum value for numbers.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.2.2
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub maximum: Option<serde_json::Number>,

    /// The maximum value for numbers, excluding itself.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.2.3
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub exclusive_maximum: Option<serde_json::Number>,

    /// The minumum value for numbers.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.2.4
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub minimum: Option<serde_json::Number>,

    /// The minimum value for numbers, excluding itself.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.2.5
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub exclusive_minimum: Option<serde_json::Number>,

    /// The maximum length of a string.
    ///
    /// The length of a string is validated as the number of its characters
    /// as defined by RFC7159.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.3.1
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub max_length: Option<usize>,

    /// The minimum length of a string.
    ///
    /// The length of a string is validated as the number of its characters
    /// as defined by RFC7159.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.3.2
    #[serde(
        default = "Default::default", skip_serializing_if = "is_default"
    )]
    pub min_length: usize,

    /// The regular expression pattern of a string.
    ///
    /// Should be a valid regular expression, according to the ECMA262
    /// regular expression dialect.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.3.3
    // TODO: format: regex
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub pattern: Option<String>,

    /// The type of array items.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.1
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub items: Option<SingleOrMultiple<Schema>>,

    /// The type of additional array items.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.2
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub additional_items: Option<Schema>,

    /// The maximum number of array items.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.3
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub max_items: Option<usize>,

    /// The minimum number of array items.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.4
    #[serde(
        default = "Default::default", skip_serializing_if = "is_default"
    )]
    pub min_items: usize,

    /// A flag indicating whether array items must be unique.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.5
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub unique_items: Option<bool>,

    /// An array must contain at least one of these items.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.4.6
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub contains: Option<Schema>,

    /// The maximum number of properties in an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.1
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub max_properties: Option<usize>,

    /// The minimum number of properties in an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.2
    #[serde(
        default = "Default::default", skip_serializing_if = "is_default"
    )]
    pub min_properties: usize,

    /// A list of required properties in an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.3
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeSet::is_empty"
    )]
    pub required: BTreeSet<String>,

    /// Schema of properties contained in an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.4
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeMap::is_empty"
    )]
    pub properties: BTreeMap<String, Schema>,

    /// Properties of an object with keys matching a regular expression.
    ///
    /// The keys of the map should be a valid regular expression in the
    /// ECMA262 dialect.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.5
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeMap::is_empty"
    )]
    // TODO: check that key is a regex
    pub pattern_properties: BTreeMap<String, Schema>,

    /// Validation of additional properties found in an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.6
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub additional_properties: Option<Schema>,

    /// Sub-schema definitions.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.9
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeMap::is_empty"
    )]
    pub definitions: BTreeMap<String, Schema>,

    /// Specific rules that are evaluated if the instance is an object
    /// and contains a certain property.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.7
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub dependencies: Option<SingleOrOtherMultiple<Schema, String>>,

    /// Property names of an object.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.5.8
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub property_names: Option<Schema>,

    /// The value must have this const value.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.1.3
    #[serde(
        rename = "const",
        default = "Default::default",
        skip_serializing_if = "Option::is_none",
        deserialize_with = "deserialize_some"
    )]
    pub const_: Option<serde_json::Value>,

    /// An enum containing valid values for the instance.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.1.2
    #[serde(
        rename = "enum",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub enum_: Option<Vec<serde_json::Value>>,

    /// The simple type(s) that are allowed for the instance.
    #[serde(
        rename = "type",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub type_: Option<SingleOrMultipleUnique<SimpleType>>,

    /// The format of the value.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.7
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub format: Option<String>,

    /// Content media type.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.8.4
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub content_media_type: Option<String>,

    /// Content encoding.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.8.3
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub content_encoding: Option<String>,

    /// Conditional evaluation of subschema.
    ///
    /// JSON object member: `if`.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.6.1
    #[serde(
        rename = "if",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub if_: Option<Schema>,

    /// Conditional evaluation of subschema.
    ///
    /// JSON object member: `then`.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.6.2
    #[serde(
        rename = "then",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub then_: Option<Schema>,

    /// Conditional evaluation of subschema.
    ///
    /// JSON object member: `else`.
    ///
    /// See: http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.6.3
    #[serde(
        rename = "else",
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub else_: Option<Schema>,

    /// Subschemas applied by boolean logic.
    ///
    /// http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.7.1
    #[serde(
        default = "Default::default", skip_serializing_if = "Vec::is_empty"
    )]
    pub all_of: Vec<Schema>,

    /// Subschemas applied by boolean logic.
    ///
    /// http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.7.2
    #[serde(
        default = "Default::default", skip_serializing_if = "Vec::is_empty"
    )]
    pub any_of: Vec<Schema>,

    /// Subschemas applied by boolean logic.
    ///
    /// http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.7.3
    #[serde(
        default = "Default::default", skip_serializing_if = "Vec::is_empty"
    )]
    pub one_of: Vec<Schema>,

    /// Subschemas applied by boolean logic.
    ///
    /// http://json-schema.org/draft-07/json-schema-validation.html#rfc.section.6.7.4
    #[serde(
        default = "Default::default",
        skip_serializing_if = "Option::is_none"
    )]
    pub not: Option<Schema>,
}

#[cfg(test)]
mod tests {
    use {SchemaDescription, SimpleType, SingleOrMultipleUnique};

    mod valid_types {
        use super::*;

        #[test]
        fn test_none() {
            let description = SchemaDescription::default();

            let expected: SingleOrMultipleUnique<SimpleType> =
                vec![
                    SimpleType::Array,
                    SimpleType::Boolean,
                    SimpleType::Integer,
                    SimpleType::Null,
                    SimpleType::Number,
                    SimpleType::Object,
                    SimpleType::String,
                ].into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_array() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Array.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::Array.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_boolean() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Boolean.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::Boolean.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_integer() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Integer.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::Integer.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_null() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Null.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::Null.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_number() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Number.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                vec![SimpleType::Number, SimpleType::Integer].into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_string() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::String.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::String.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_object() {
            let mut description = SchemaDescription::default();
            description.type_ = Some(SimpleType::Object.into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                SimpleType::Object.into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_array_and_object() {
            let mut description = SchemaDescription::default();
            description.type_ =
                Some(vec![SimpleType::Array, SimpleType::Object].into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                vec![SimpleType::Array, SimpleType::Object].into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_string_and_boolean() {
            let mut description = SchemaDescription::default();
            description.type_ =
                Some(vec![SimpleType::String, SimpleType::Boolean].into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                vec![SimpleType::String, SimpleType::Boolean].into();
            assert_eq!(description.valid_types(), expected);
        }

        #[test]
        fn test_null_and_number() {
            let mut description = SchemaDescription::default();
            description.type_ =
                Some(vec![SimpleType::Null, SimpleType::Number].into());

            let expected: SingleOrMultipleUnique<SimpleType> =
                vec![
                    SimpleType::Null,
                    SimpleType::Number,
                    SimpleType::Integer,
                ].into();
            assert_eq!(description.valid_types(), expected);
        }
    }
}
